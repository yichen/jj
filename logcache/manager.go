package logcache

import "bitbucket.org/yichen/jj/cluster/core"

type LogType string

var (
	LogTypeController LogType = "controller.gz"
	LogTypeStdOut     LogType = "stdout.gz"
	LogTypeStdErr     LogType = "stderr.gz"
)

type Manager struct {
	zkConn   *core.Connection
	Capacity int64
	Location map[string]map[string]string
}
