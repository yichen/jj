package main

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"bitbucket.org/yichen/jj/common"
	"github.com/go-resty/resty"
	"github.com/spf13/viper"
	"github.com/urfave/cli"
)

/*

message DescribeJobResponse {
    string job = 1;
    string cluster = 2;
    string step = 3;
    string url = 4;
    string state = 5;
}
*/
type jobResponse struct {
	Job     string
	Cluster string
	Step    string
	Url     string
	State   string
}

/*
message Cluster {
    string Id = 1;
    string Name = 2;
    int64 NormalizedInstanceHours = 3;
    string Status = 4;
}
*/

type clusterResponse struct {
	Id                      string
	Name                    string
	NormalizedInstanceHours string
	Status                  string
}

type clustersResponse struct {
	Clusters []clusterResponse
}

type jobsResponse struct {
	Jobs []jobResponse
}

var ListCommand = cli.Command{
	Name:    "list",
	Aliases: []string{"ls"},
	Usage:   "list jobs or clusters",
	Flags: []cli.Flag{
		cli.BoolFlag{
			Name:  "active",
			Usage: "list only clusters that are STARTING, BOOTSTRAPPING, RUNNING, WAITING, or TERMINATING",
		},
		cli.BoolFlag{
			Name:  "failed",
			Usage: "list only clusters that are TERMINATED_WITH_ERRORS",
		},
		cli.BoolFlag{
			Name:  "terminated",
			Usage: "list only clusters that are TERMINATED",
		},
	},

	Action: func(c *cli.Context) error {
		debug := c.GlobalBool("debug")

		if c.NArg() != 1 {
			fmt.Println("Usage: j list [CLUSTERS|JOBS]")
			return nil
		}

		objectName := strings.ToLower(c.Args().First())

		common.ReadConfig()
		server := viper.Get("server")
		if debug {
			fmt.Println("DEBUG server in config file: ", server)
		}
		if c.GlobalString("server") != "" {
			server = c.GlobalString("server")
			fmt.Println("DEBUG override server from --server option: ", server)
		}
		if server == nil || server == "" {
			fmt.Println("Use j config server HOST:PORT first to point to the backend")
			return nil
		}

		endpoint := server.(string)
		if !strings.HasPrefix(endpoint, "http") {
			endpoint = "http://" + endpoint
		}
		if !strings.HasSuffix(endpoint, "/") {
			endpoint = endpoint + "/"
		}

		switch objectName {
		case "job", "jobs":
			endpoint += "v1/jobs"
			if debug {
				fmt.Println("DEBUG REST endpoint: ", endpoint)
			}

			resp, err := resty.R().Get(endpoint)
			if err != nil {
				fmt.Printf("\nError: %v", err)
				return err
			}
			if debug {
				fmt.Printf("DEBUG responseBody: %v", resp)
			}

			jobs := jobsResponse{}
			err = json.Unmarshal(resp.Body(), &jobs)
			if err != nil {
				fmt.Printf("Error: %s", err)
				return err
			}

			format := "%-20s%-20s%-20s%-20s\n"
			fmt.Printf(format, "JOB ID", "CLUSTER", "STEP", "STATE")

			for _, j := range jobs.Jobs {
				fmt.Printf(format, j.Job, j.Cluster, j.Step, j.State)
			}

		case "cluster", "clusters":
			endpoint += "v1/clusters"

			if c.Bool("active") || c.Bool("failed") || c.Bool("terminated") {
				u, _ := url.Parse(endpoint)
				q := u.Query()
				q.Set("active", strconv.FormatBool(c.Bool("active")))
				q.Set("failed", strconv.FormatBool(c.Bool("failed")))
				q.Set("terminated", strconv.FormatBool(c.Bool("terminated")))
				u.RawQuery = q.Encode()
				endpoint = u.String()
			}
			if debug {
				fmt.Println("DEBUG REST endpoint: ", endpoint)
			}

			resp, err := resty.R().Get(endpoint)
			if err != nil {
				fmt.Printf("\nError: %v", err)
				return err
			}
			if debug {
				fmt.Printf("DEBUG responseBody: %v", resp)
			}

			clusters := clustersResponse{}
			err = json.Unmarshal(resp.Body(), &clusters)
			if err != nil {
				fmt.Printf("Error: %s", err)
				return err
			}

			format := "%-30s%-30s\n"
			fmt.Printf(format, "CLUSTER ID", "NAME")
			for _, cl := range clusters.Clusters {
				fmt.Printf(format, cl.Id, cl.Name)
			}

		default:
			fmt.Println("Usage: j list [CLUSTERS|JOBS]")
			return nil
		}

		//server := "i-0fd0936a9272c3418.inst.aws.us-east-1.prod.musta.ch:20000"

		return nil
	},
}
