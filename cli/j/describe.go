package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/yichen/jj/common"
	"github.com/go-resty/resty"
	"github.com/spf13/viper"
	"github.com/urfave/cli"
)

/*
message DescribeJobResponse {
    string job = 1;
    string cluster = 2;
    string step = 3;
    string url = 4;
    string state = 5;
}
*/
type describeJobResponse struct {
	Job     string `json:"job"`
	Cluster string `json:"cluster"`
	Step    string `json:"step"`
	Url     string `json:"url"`
	State   string `json:"state"`
}

/*
message DescribeClusterResponse {
    string cluster = 1;
    string name = 2;
    repeated string applications = 3;
    string autoScalingRole = 4;
    bool autoTerminate = 5;
    string logUri = 6;
    int64 normalizedInstanceHours = 7;
    string releaseLabel = 8;
    string runningAmiVersion = 9;
    string state = 10;
    string masterPublicDnsName = 11;
    string masterInstanceType = 12;
    int64 masterRunningInstanceCount = 13;
    string coreInstanceType = 14;
    int64 coreRunningInstanceCount = 15;
    string scaleDownBehavior = 16;
    string instanceCollectionType = 17;
}
*/

type describeClusterResponse struct {
	Cluster                    string   `json:"cluster"`
	Name                       string   `json:"name"`
	Applications               []string `json:"applications"`
	AutoScalingRole            string   `json:"autoScalingRole"`
	AutoTerminate              bool     `json:"autoTerminate"`
	LogUrl                     string   `json:"logUri"`
	NormalizedInstanceHours    string   `json:"normalizedInstanceHours"`
	ReleaseLabel               string   `json:"releaseLabel"`
	State                      string   `json:"state"`
	MasterInstanceType         string   `json:"masterInstanceType"`
	MasterRunningInstanceCount string   `json:"masterRunningInstanceCount"`
	CoreInstanceType           string   `json:"coreInstanceType"`
	CoreRunningInstanceCount   string   `json:"coreRunningInstanceCount"`
	ScaleDownBehavior          string   `json:"scaleDownBehavior"`
	InstanceCollectionType     string   `json:"instanceCollectionType"`
}

var DescribeCommand = cli.Command{
	Name:    "describe",
	Aliases: []string{"desc"},
	Usage:   "describe a job",
	Flags:   []cli.Flag{},

	Action: func(c *cli.Context) error {

		if c.NArg() == 0 {
			fmt.Println("Usage: j describe [OBJECT_NAME] [OBJECT_ID]")
			return nil
		}

		debug := c.GlobalBool("debug")
		objectName := strings.ToLower(c.Args().First())

		common.ReadConfig()
		server := viper.Get("server")
		if debug {
			fmt.Println("DEBUG server in config file: ", server)
		}
		if c.GlobalString("server") != "" {
			server = c.GlobalString("server")
			fmt.Println("DEBUG override server from --server option: ", server)
		}

		if server == nil {
			fmt.Println("Use j config server HOST:PORT first to point to the backend")
			return nil
		}

		endpoint := server.(string)
		if !strings.HasPrefix(endpoint, "http") {
			endpoint = "http://" + endpoint
		}
		if !strings.HasSuffix(endpoint, "/") {
			endpoint = endpoint + "/"
		}

		switch objectName {
		case "job":
			if c.NArg() != 2 {
				fmt.Println("Usage: jj describe job [JOB_ID]")
				return nil
			}

			jobID := c.Args().Get(1)
			endpoint += "v1/jobs/" + jobID
			if debug {
				fmt.Printf("DEBUG endpoint:%s", endpoint)
			}

			resp, err := resty.R().Get(endpoint)
			if err != nil {
				fmt.Printf("\nError: %v", err)
				return err
			}

			jr := describeJobResponse{}
			err = json.Unmarshal(resp.Body(), &jr)
			if err != nil {
				fmt.Printf("Error: %s", err)
				return err
			}

			if debug {
				fmt.Printf("DEBUG ResponseBody:%v", jr)
			}

			format := "%-20s%-80s\n"

			fmt.Printf(format, "ID", jr.Job)
			fmt.Printf(format, "Cluster", jr.Cluster)
			fmt.Printf(format, "Step", jr.Step)
			fmt.Printf(format, "Url", jr.Url)
			fmt.Printf(format, "State", jr.State)

		case "cluster":
			if c.NArg() != 2 {
				fmt.Println("Usage: j describe cluster [JOB_ID]")
				return nil
			}

			clusterID := c.Args().Get(1)
			endpoint += "v1/clusters/" + clusterID
			if debug {
				fmt.Printf("DEBUG endpoint:%s", endpoint)
			}

			resp, err := resty.R().Get(endpoint)
			if err != nil {
				fmt.Printf("\nError: %v", err)
				return err
			}

			jr := describeClusterResponse{}
			err = json.Unmarshal(resp.Body(), &jr)
			if err != nil {
				fmt.Printf("Error: %s", err)
				return err
			}

			if debug {
				fmt.Printf("DEBUG ResponseBody:%v", jr)
			}

			format := "%-30s%-80s\n"

			fmt.Printf(format, "Cluster", jr.Cluster)
			fmt.Printf(format, "Name", jr.Name)
			fmt.Printf(format, "Applications", " ")
			for _, app := range jr.Applications {
				fmt.Printf(format, " ", app)
			}
			fmt.Printf(format, "AutoScalingRole", jr.AutoScalingRole)
			fmt.Printf(format, "AutoTerminate", strconv.FormatBool(jr.AutoTerminate))
			fmt.Printf(format, "LogUri", jr.LogUrl)
			fmt.Printf(format, "NormalizedInstanceHours", jr.NormalizedInstanceHours)
			fmt.Printf(format, "ReleaseLabel", jr.ReleaseLabel)
			fmt.Printf(format, "MasterInstanceType", jr.MasterInstanceType)
			fmt.Printf(format, "MasterRunningInstanceCount", jr.MasterRunningInstanceCount)
			fmt.Printf(format, "CoreInstnaceType", jr.CoreInstanceType)
			fmt.Printf(format, "CoreRunningInstanceCount", jr.CoreRunningInstanceCount)
			fmt.Printf(format, "ScaleDownBehavior", jr.ScaleDownBehavior)
			fmt.Printf(format, "InstanceCollectionType", jr.InstanceCollectionType)
			fmt.Printf(format, "State", jr.State)
		}

		return nil
	},
}
