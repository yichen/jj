package main

import (
	"fmt"
	"path"
	"strings"

	"bitbucket.org/yichen/jj/common"
	"github.com/spf13/viper"
	"github.com/urfave/cli"
)

var ConfigCommand = cli.Command{
	Name:  "config",
	Usage: "Configure the server endpoint",
	Subcommands: []cli.Command{
		{
			Name:  "env",
			Usage: "jj config env [production|staging]",
			Action: func(c *cli.Context) error {
				common.ReadConfig()

				if c.NArg() == 0 {
					fmt.Printf("Environment\t: %s\n", viper.Get("env"))
					fmt.Printf("Backend\t\t: %s\n", viper.Get("backend_master"))
					fmt.Printf("       \t\t: %s\n", viper.Get("backend_worker"))
					return nil
				}

				if c.NArg() != 1 {
					fmt.Println("invalid arguments")
					return nil
				}

				env := strings.TrimSpace(c.Args().First())
				if env != "production" && env != "prod" && env != "stage" && env != "staging" {
					fmt.Println("invalid env argument. must be either 'production' or 'staging'")
					return nil
				}
				fmt.Printf("configuring environment to %s\n", env)

				if env == "production" || env == "prod" {
					viper.Set("env", "production")
					viper.Set("backend_worker", common.BACKEND_WORKER_PRODUCTION)
					viper.Set("backend_master", common.BACKEND_MASTER_PRODUCTION)
				} else if env == "stage" || env == "staging" {
					viper.Set("env", "staging")
					viper.Set("backend_worker", common.BACKEND_WORKER_STAGING)
					viper.Set("backend_master", common.BACKEND_MASTER_STAGING)
				}

				viper.WriteConfigAs(path.Join(common.GetConfigPath(), "config.json"))

				return nil
			},
		},
	},

	Action: func(c *cli.Context) error {
		return nil
	},
}
