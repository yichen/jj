package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/yichen/jj/common"
	"bitbucket.org/yichen/jj/models"
	"github.com/go-resty/resty"
	"github.com/spf13/viper"
	"github.com/urfave/cli"
)

func getEmrClustersFromAWSAPI(backend string) models.EmrClustersAdmin {

	endpoint := fmt.Sprintf("%s/v1/jobserver/clusters", backend)

	resp, err := resty.R().Get(endpoint)
	if err != nil {
		fmt.Printf("failed to get data from %s\n", endpoint)
		return nil
	}

	emrclusters := models.EmrClustersAdmin{}
	err = json.Unmarshal(resp.Body(), &emrclusters)
	if err != nil {
		fmt.Printf("failed to parse result from %s\n", endpoint)
		return nil
	}

	return emrclusters
}

func getEmrClustersFromPublicEndpoint(backend string) *models.EmrClustersPublic {

	endpoint := fmt.Sprintf("%s/v1/clusters?state=active", backend)

	resp, err := resty.R().Get(endpoint)
	if err != nil {
		fmt.Printf("failed to get data from %s\n", endpoint)
		return nil
	}

	emrclusters := models.EmrClustersPublic{}
	err = json.Unmarshal(resp.Body(), &emrclusters)
	if err != nil {
		fmt.Printf("failed to parse result from %s\n", endpoint)
		return nil
	}
	return &emrclusters
}

func getWorkerAssignment(backend string) *models.WorkerAssignment {
	endpoint := fmt.Sprintf("%s/v1/jobserver/assignment", backend)

	resp, err := resty.R().Get(endpoint)
	if err != nil {
		fmt.Printf("failed to get data from %s\n", endpoint)
		return nil
	}

	assignment := models.WorkerAssignment{}
	err = json.Unmarshal(resp.Body(), &assignment)
	if err != nil {
		fmt.Printf("failed to parse result from %s\n", endpoint)
		return nil
	}
	return &assignment
}

func getTaskState(backend string, taskId string) *models.TaskState {
	endpoint := fmt.Sprintf("%s/v1/jobserver/tasks/%s", backend, taskId)
	resp, err := resty.R().Get(endpoint)
	if err != nil {
		fmt.Printf("failed to get data from %s\n", endpoint)
		return nil
	}

	taskState := models.TaskState{}
	err = json.Unmarshal(resp.Body(), &taskState)
	if err != nil {
		fmt.Printf("failed to parse result from %s\n", endpoint)
		return nil
	}
	return &taskState
}

var DoctorCommand = cli.Command{
	Name:  "doctor",
	Aliases: []string{"d"},
	Usage: "diagnose the cluster/job",
	Subcommands: []cli.Command{
		{
			Name:  "jobserver",
			Usage: "jj doctor jobserver",
			Action: func(c *cli.Context) error {
				common.ReadConfig()

				fmt.Printf("Environment\t: %s\n", viper.Get("env"))
				fmt.Printf("Backend\t\t: %s\n", viper.Get("backend_master"))
				fmt.Printf("       \t\t: %s\n", viper.Get("backend_worker"))

				return nil
			},
		},
	},

	Action: func(c *cli.Context) error {
		common.ReadConfig()
		titleFormat := "%-20s "

		fmt.Printf(titleFormat+"%s\n", "Environment", viper.Get("env"))
		fmt.Printf(titleFormat+"%s\n", "Backend", viper.Get("backend_master"))
		fmt.Printf(titleFormat+"%s\n", " ", viper.Get("backend_worker"))

		backend := viper.Get("backend_worker").(string)
		clusters := getEmrClustersFromAWSAPI(backend)

		clustersFromPublicEndpoint := getEmrClustersFromPublicEndpoint(backend)
		if clustersFromPublicEndpoint == nil {
			return nil
		}

		fmt.Printf(titleFormat+"%d\n", "Clusters", len(clusters))
		if len(clusters) != len(clustersFromPublicEndpoint.Data) {
			s1 := common.NewLinkedSet()
			for _, c := range clusters {
				s1.Push(c.ID)
			}

			s2 := common.NewLinkedSet()
			for _, c := range clustersFromPublicEndpoint.Data {
				s2.Push(c.ID)
			}

			diff1 := s1.RemoveAll(s2)
			diff2 := s2.RemoveAll(s1)

			if diff1.Size() != 0 || diff2.Size() != 0 {
				fmt.Printf("WARN: active cluster list is not accurate\n")
				if diff1.Size() > 0 {
					fmt.Printf("+++ %s\n", strings.Join(diff1.GetAll(), ", "))
				}
				if diff2.Size() > 0 {
					fmt.Printf("--- %s\n", strings.Join(diff2.GetAll(), ", "))
				}
			}

		}

		taskList := []string{
			"com.airbnb.compute.jobs.worker.tasks.FetchAllRecentApplicationsTask",
			"com.airbnb.compute.jobs.worker.tasks.LogDirCleanUpTask",
			"com.airbnb.compute.jobs.worker.tasks.WorkerStateUpdater",
			"com.airbnb.compute.jobs.worker.tasks.UpdateSparkInsightTask",
			"com.airbnb.compute.jobs.worker.tasks.IngestTezDagDataFromATS",
			"com.airbnb.compute.jobs.master.tasks.ActiveClusterUpdatesTask",
			"com.airbnb.compute.jobs.master.tasks.EmrClusterChangeMonitor",
		}

		for i, task := range taskList {
			state1 := getTaskState(backend, task)
			if state1 == nil {
				return nil
			}

			if state1.SimpleFields.LASTRUNENDAT == "" {
				fmt.Printf(titleFormat+"%-72s%-12s  NO DATA\n", " ", task, state1.SimpleFields.LASTRUNRESULT)
				continue
			}

			lastRunAt, err := strconv.ParseInt(state1.SimpleFields.LASTRUNENDAT, 10, 64)
				if err != nil {
					panic(err)
				}

			now := time.Now().UnixNano() / 1000000
			seconds := (now - lastRunAt) / 1000
			minutes := seconds / 60
			hours := minutes / 60

			space := " "
			if i == 0 {
				space = "Tasks"
			}

			if hours > 2 {
				fmt.Printf(titleFormat+"%-72s%-12s  %d hours ago\n", space, task, state1.SimpleFields.LASTRUNRESULT, hours)
			} else if minutes > 2 {
				fmt.Printf(titleFormat+"%-72s%-12s  %d minutes ago\n", space, task, state1.SimpleFields.LASTRUNRESULT, minutes)
			} else {
				fmt.Printf(titleFormat+"%-72s%-12s  %d seconds ago\n", space, task, state1.SimpleFields.LASTRUNRESULT, seconds)
			}

			if state1.SimpleFields.LASTRUNRESULT != "SUCCESS" && len(state1.SimpleFields.LASTRUNEXCEPTION) > 1 {
				fmt.Printf(titleFormat+"%-72s  %s\n", " ", " ", state1.SimpleFields.LASTRUNEXCEPTION)
			}

		}

		assignment := getWorkerAssignment(backend)
		if assignment == nil {
			return nil
		}

		i := 0
		space := "Assignment"
		for w, a := range assignment.ListFields {
			if i > 0 {
				space = " "
			}
			i++
			fmt.Printf(titleFormat+"%-72s%-12d\n", space, w[23:len(w)-5], len(a))
		}

		return nil
	},
}
