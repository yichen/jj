package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/yichen/jj/common"
	"github.com/spf13/viper"
	"github.com/urfave/cli"
)

type logContent struct {
	Content string `json:"content"`
}
type logResponse struct {
	Result logContent `json:"result"`
}

var LogsCommand = cli.Command{
	Name:        "logs",
	Aliases:     []string{"log"},
	Usage:       "j logs JOB_ID",
	Description: "Retrieve logs for a job",
	Flags: []cli.Flag{
		cli.BoolFlag{
			Name:  "driver",
			Usage: "retrieve only driver logs",
		},
		cli.BoolFlag{
			Name:  "executor",
			Usage: "retrieve only executor logs",
		},
	},

	Action: func(c *cli.Context) error {

		if c.NArg() != 1 {
			fmt.Println("Usage: jj logs [JOB_ID]")
			return nil
		}

		debug := c.GlobalBool("debug")
		job := c.Args().First()

		common.ReadConfig()
		server := viper.Get("server")
		if server == nil {
			fmt.Println("Use j config server HOST:PORT first to point to the backend")
			return nil
		}
		if c.GlobalString("server") != "" {
			server = c.GlobalString("server")
		}

		endpoint := server.(string)
		if !strings.HasPrefix(endpoint, "http") {
			endpoint = "http://" + endpoint
		}
		if !strings.HasSuffix(endpoint, "/") {
			endpoint = endpoint + "/"
		}

		endpoint += "v1/logs/" + job
		if debug {
			fmt.Printf("DEBUG endpoint: %s\n", endpoint)
		}

		client := http.Client{}
		req, err := http.NewRequest(http.MethodGet, endpoint, nil)
		if err != nil {
			return err
		}
		req.Header.Set("User-Agent", "jobserver-j")
		res, err := client.Do(req)
		if err != nil {
			return err
		}
		defer res.Body.Close()

		if debug {
			fmt.Printf("DEBUG response status: %s\n", res.Status)
		}

		for err != io.EOF {
			lr := logResponse{}
			err = json.NewDecoder(res.Body).Decode(&lr)
			if lr.Result.Content == "" {
				time.Sleep(10 * time.Millisecond)
			}
			fmt.Printf("%v\n", lr.Result.Content)
		}
		if err != nil && err != io.EOF {
			fmt.Println("Error: ", err.Error())
		}

		return nil
	},
}
