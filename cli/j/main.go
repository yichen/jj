package main

import (
	"os"

	"bitbucket.org/yichen/jj/common"
	"github.com/urfave/cli"
)

func main() {
	pp := JobServer()
	err := pp.Run(os.Args)
	if err != nil {
		os.Exit(1)
	}
}

func JobServer() *cli.App {
	app := cli.NewApp()
	app.Name = "JustJobs"
	app.Description = "JobServer CLI"
	app.Usage = "j"
	app.Version = common.Version
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "server",
			Usage: "HTTP API endpoint in the form of host:port",
		},
		cli.BoolFlag{
			Name:  "debug",
			Usage: "generate debug output",
		},
	}

	app.Commands = []cli.Command{
		VersionCommand,
		ConfigCommand,
		ListCommand,
		LogsCommand,
		DescribeCommand,
		DoctorCommand,
	}

	return app
}
