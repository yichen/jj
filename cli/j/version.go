package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/go-resty/resty"
	"github.com/spf13/viper"

	"bitbucket.org/yichen/jj/common"
	"github.com/urfave/cli"
)

type healthResponse struct {
	Version string
}

var VersionCommand = cli.Command{
	Name:  "version",
	Usage: "show the version of J CLI",
	Flags: []cli.Flag{
		cli.BoolFlag{
			Name:  "remote",
			Usage: "list the remote version",
		},
	},
	Action: func(c *cli.Context) error {
		if !c.Bool("remote") {
			fmt.Println(common.Version)
			return nil
		}

		common.ReadConfig()
		server := viper.Get("server")
		if server == nil {
			fmt.Println("Use j config server HOST:PORT first to point to the backend")
			return nil
		}

		endpoint := server.(string)
		if !strings.HasPrefix(endpoint, "http") {
			endpoint = "http://" + endpoint
		}
		if !strings.HasSuffix(endpoint, "/") {
			endpoint = endpoint + "/"
		}

		endpoint += "health"

		resp, err := resty.R().Get(endpoint)
		if err != nil {
			fmt.Printf("\nError: %v", err)
			return err
		}

		health := healthResponse{}
		json.Unmarshal(resp.Body(), &health)

		fmt.Println(health.Version)

		return nil
	},
}
