package main

import (
	"os"

	"bitbucket.org/yichen/jj/common"
	"github.com/urfave/cli"
)

func main() {
	app := JobServer()
	err := app.Run(os.Args)
	if err != nil {
		os.Exit(1)
	}
}

func JobServer() *cli.App {
	app := cli.NewApp()
	app.Name = "JobServer CLI"
	app.Description = "CLI for JobServer via gRPC"
	app.Usage = "jj"
	app.Version = common.Version
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "server",
			Usage: "HTTP API endpoint in the form of host:port",
		},
		cli.BoolFlag{
			Name:  "debug",
			Usage: "generate debug output",
		},
	}

	app.Commands = []cli.Command{
		VersionCommand,
		ServerCommand,
		ConfigCommand,
		DescribeCommand,
		ListCommand,
		LogsCommand,
		CreateClusterCommand,
	}
	return app
}
