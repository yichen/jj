package main

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/yichen/jj/common"
	"bitbucket.org/yichen/jj/pb"
	"github.com/spf13/viper"
	"github.com/urfave/cli"
)

var CreateCommand = cli.Command{
	Name:    "create",
	Usage:   "create new jobs or clusters",
	Flags: []cli.Flag{},

	Action: func(c *cli.Context) error {

		if c.NArg() == 0 {
			fmt.Println("Usage: jj create [OBJECT_NAME]")
			return nil
		}

		debug := c.GlobalBool("debug")
		common.ReadConfig()
		server := viper.Get("server")
		if server == nil {
			fmt.Println("Use j config server HOST:PORT first to point to the backend")
			return nil
		}
		if debug {
			fmt.Printf("DEBUG server endpoint from config file: %s\n", server)
		}
		if c.GlobalString("server") != "" {
			server = c.GlobalString("server")
			if debug {
				fmt.Printf("DEBUG override server endpoint from commandline option: %s\n", server)
			}
		}

		objectName := strings.ToUpper(c.Args().First())
		switch objectName {
		case "JOB", "JOBS":
			if c.NArg() != 1 {
				fmt.Println("Usage: jj list job")
				return nil
			}

			return CallJJService(server.(string), func(ctx context.Context, cli pb.JJServiceClient) error {
				req := pb.Empty{}
				resp, err := cli.ListJobs(ctx, &req)
				if err != nil {
					fmt.Printf("ERROR cli.ListJobs. Eror: %s\n", err.Error())
					return err
				}

				if resp == nil {
					return nil
				}

				fmt.Printf("%-3s  %-20s %-15s %-20s %-12s\n", "", "JOB", "CLUSTER", "STEP", "STATE")
				format := "%-3d  %-20s %-15s %-20s %-12s\n"
				for i, j := range resp.Jobs {
					fmt.Printf(format, i, j.GetJob(), j.GetCluster(), j.GetStep(), j.GetState())
				}

				return nil
			})
		case "CLUSTER", "CLUSTERS":
			return CallJJService(server.(string), func(ctx context.Context, cli pb.JJServiceClient) error {
				req := pb.CreateClusterRequest{}
				_, err := cli.CreateCluster(ctx, &req)
				if err != nil {
					fmt.Printf("ERROR cli.ListClusters Error: %s\n", err.Error())
					return err
				}

				return nil
			})
		}

		return nil
	},
}

