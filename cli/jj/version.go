package main

import (
	"fmt"

	"bitbucket.org/yichen/jj/common"
	"github.com/urfave/cli"
)

var VersionCommand = cli.Command{
	Name:  "version",
	Usage: "show the version of JobServer",
	Action: func(c *cli.Context) error {
		fmt.Println(common.Version)
		return nil
	},
}
