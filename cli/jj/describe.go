package main

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/yichen/jj/common"
	"bitbucket.org/yichen/jj/pb"
	"github.com/spf13/viper"
	"github.com/urfave/cli"
)

var DescribeCommand = cli.Command{
	Name:    "describe",
	Aliases: []string{"desc"},
	Usage:   "describe a job",
	Flags:   []cli.Flag{},

	Action: func(c *cli.Context) error {

		if c.NArg() == 0 {
			fmt.Println("Usage: jj describe [OBJECT_NAME] [OBJECT_ID]")
			return nil
		}

		common.ReadConfig()
		server := viper.Get("server")
		if server == nil {
			fmt.Println("Use j config server HOST:PORT first to point to the backend")
			return nil
		}

		objectName := strings.ToUpper(c.Args().First())

		switch objectName {
		case "JOB":
			if c.NArg() != 2 {
				fmt.Println("Usage: jj describe job [JOB_ID]")
				return nil
			}

			jobID := c.Args().Get(1)

			return CallJJService(server.(string), func(ctx context.Context, cli pb.JJServiceClient) error {
				req := pb.DescribeJobRequest{Job: jobID}
				resp, err := cli.DescribeJob(ctx, &req)
				if err != nil {
					fmt.Printf("ERROR: %s", err.Error())
					return err
				}

				format := "%-10s:  %s\n"

				fmt.Printf(format, "JOB", resp.Job)
				fmt.Printf(format, "URL", resp.Url)
				fmt.Printf(format, "STATE", resp.State)
				fmt.Printf(format, "CLUSTER", resp.Cluster)
				fmt.Printf(format, "STEP", resp.Step)

				return nil
			})
		case "CLUSTER":
			if c.NArg() != 2 {
				fmt.Println("Usage: jj describe cluster [JOB_ID]")
				return nil
			}

			clusterID := c.Args().Get(1)

			return CallJJService(server.(string), func(ctx context.Context, cli pb.JJServiceClient) error {
				req := pb.DescribeClusterRequest{Cluster: clusterID}
				resp, err := cli.DescribeCluster(ctx, &req)
				if err != nil {
					fmt.Printf("ERROR: %s", err.Error())
					return err
				}

				format := "%-27s:  %s\n"

				fmt.Printf(format, "ID", resp.Cluster)
				fmt.Printf(format, "Name", resp.Name)
				fmt.Printf(format, "Applications", resp.Applications)
				fmt.Printf(format, "NormalizedInstanceHours", strconv.FormatInt(resp.NormalizedInstanceHours, 10))
				fmt.Printf(format, "ReleaseLabel", resp.ReleaseLabel)
				fmt.Printf(format, "MasterPublicDnsName", resp.MasterPublicDnsName)
				fmt.Printf(format, "AutoTerminate", strconv.FormatBool(resp.AutoTerminate))
				fmt.Printf(format, "ScaleDownBehavior", resp.ScaleDownBehavior)
				fmt.Printf(format, "InstanceCollectionType", resp.InstanceCollectionType)
				fmt.Printf(format, "MasterInstanceType", resp.MasterInstanceType)
				fmt.Printf(format, "MasterRunningInstanceCount", strconv.FormatInt(resp.MasterRunningInstanceCount, 10))
				fmt.Printf(format, "CoreInstanceType", resp.CoreInstanceType)
				fmt.Printf(format, "CoreRunningInstanceCount", strconv.FormatInt(resp.CoreRunningInstanceCount, 10))
				fmt.Printf(format, "State", resp.State)

				return nil
			})

		}

		return nil
	},
}
