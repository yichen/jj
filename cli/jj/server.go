package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/yichen/jj/server"
	"github.com/urfave/cli"
)

var ServerCommand = cli.Command{
	Name:  "server",
	Usage: "start the JJ Server",
	Flags: []cli.Flag{
		cli.IntFlag{
			Name:  "port,p",
			Value: 20000,
		},

		cli.IntFlag{
			Name:  "httpport",
			Value: 20080,
		},

		cli.StringFlag{
			Name:  "name,n",
			Value: GetHostName(),
			Usage: "Unique name for the instance",
		},

		cli.StringFlag{
			Name:  "cluster,c",
			Value: "jobserver",
			Usage: "Unique jobserver cluster this instance belongs to",
		},

		cli.StringFlag{
			Name:  "zookeeper,zk",
			Value: "zk-druid-0.silver.musta.ch:2181,zk-druid-1.silver.musta.ch:2181,zk-druid-2.silver.musta.ch:2181,zk-druid-3.silver.musta.ch:2181,zk-druid-4.silver.musta.ch:2181",
			Usage: "Zookeeper connection string",
		},
	},

	Action: func(c *cli.Context) error {
		port := c.Int("port")
		httpPort := c.Int("httpport")
		name := c.String("name")
		cluster := c.String("cluster")
		zkAddr := c.String("zookeeper")

		log.Printf("Starting Jobs service: %s, gRPC port: %d\n", name, port)
		log.Printf("Cluster Name: %s, ZooKeeper: %s\n", cluster, zkAddr)

		s := server.NewServer(zkAddr, cluster, name, port, httpPort)
		go s.Start()

		// wait for shutdown signal
		sig := make(chan os.Signal)
		signal.Notify(sig, os.Interrupt, syscall.SIGTERM)

		<-sig
		log.Println("Shutting down...")
		s.Stop()
		log.Println("Bye!")

		return nil
	},
}

func GetHostName() string {
	host, _ := os.Hostname()
	if host == "" {
		host = "host"
	}

	return host
}
