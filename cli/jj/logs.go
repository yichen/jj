package main

import (
	"context"
	"fmt"
	"io"
	"time"

	"bitbucket.org/yichen/jj/common"
	"bitbucket.org/yichen/jj/pb"
	"github.com/spf13/viper"
	"github.com/urfave/cli"
	"google.golang.org/grpc"
)

var LogsCommand = cli.Command{
	Name:        "logs",
	Aliases:     []string{"log"},
	Usage:       "jj logs JOB_ID",
	Description: "Retrieve logs for jobs.",
	Flags: []cli.Flag{
		cli.BoolFlag{
			Name:  "driver",
			Usage: "retrieve only driver logs",
		},
		cli.BoolFlag{
			Name:  "executor",
			Usage: "retrieve only executor logs",
		},
	},

	Action: func(c *cli.Context) error {

		if c.NArg() != 1 {
			fmt.Println("Usage: jj logs [JOB_ID]")
			return nil
		}

		debug := c.GlobalBool("debug")

		job := c.Args().First()

		common.ReadConfig()

		server := viper.Get("server")
		if debug {
			fmt.Println("DEBUG server in config file: ", server)
		}
		if c.GlobalString("server") != "" {
			server = c.GlobalString("server")
			fmt.Println("DEBUG override server from --server option: ", server)
		}

		return CallJJService(server.(string), func(ctx context.Context, cli pb.JJServiceClient) error {
			req := pb.GetLogRequest{
				Job:      job,
				Driver:   c.Bool("driver"),
				Executor: c.Bool("executor"),
			}
			stream, err := cli.GetLog(ctx, &req)
			if err != nil {
				return err
			}

			for {
				log, err := stream.Recv()
				if err == io.EOF {
					break
				}

				if err != nil {
					return err
				}

				fmt.Println(log.Content)
			}
			return nil
		})
	},
}

func CallJJService(server string, f func(ctx context.Context, cli pb.JJServiceClient) error) error {
	conn, err := grpc.Dial(server, grpc.WithInsecure())
	if err != nil {
		return err
	}
	defer conn.Close()

	cli := pb.NewJJServiceClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Hour)
	defer cancel()
	err = f(ctx, cli)

	return err
}
