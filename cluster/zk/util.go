package zk

import (
	"log"
	"path"
	"strings"

	"bitbucket.org/yichen/jj/cluster/core"
	"bitbucket.org/yichen/jj/cluster/model"
	"github.com/pkg/errors"
)

var keys = core.KeyBuilder{ClusterID: "jobserver"}

func CreateJob(conn *core.Connection, cluster string) (string, error) {
	log.Printf("BEGIN CreateJob")
	defer log.Printf("END CreateJob")

	if conn == nil {
		return "", errors.New("Connection cann't be nil.")
	}

	r := model.NewJobRecord(cluster)

	var kp string
	k := keys.Jobs()
	if !strings.HasSuffix(k, "/") {
		kp = k + "/"
	} else {
		kp = k
	}

	id, err := conn.CreateSequentialRecordWithPath(kp, &r.Record)
	jobID := path.Base(id)
	return jobID, err
}

func UpdateJob(conn *core.Connection, jobID string, properties map[string]string) error {
	log.Printf("BEGIN UpdateJob: %s", jobID)
	defer log.Printf("END UpdateJob: %s", jobID)

	if conn == nil {
		return errors.New("Connection cannot be nil.")
	}

	k := keys.Job(jobID)

	r, err := conn.GetRecordFromPath(k)
	if err != nil {
		log.Printf("ERROR UpdateJob: %s", err.Error())
		return err
	}

	jr := model.JobRecord{Record: *r}

	for k, v := range properties {
		println("SET k=", k, ", v=", v)
		jr.SetSimpleField(k, v)
	}

	return conn.SetRecordForPath(k, &jr.Record)
}

func GetJob(conn *core.Connection, jobID string) (*model.JobRecord, error) {
	log.Printf("BEGIN GetJob: %s", jobID)
	defer log.Printf("END GetJob: %s", jobID)

	if conn == nil {
		return nil, errors.New("Connection cann't be nil.")
	}

	k := keys.Job(jobID)
	r, err := conn.GetRecordFromPath(k)
	if err != nil {
		log.Printf("ERROR GetJob: %s", err.Error())
		return nil, err
	}

	result := model.JobRecord{Record: *r}
	return &result, nil
}

func ListJobs(conn *core.Connection) ([]*model.JobRecord, error) {
	log.Println("BEGIN ListJobs")
	defer log.Println("END ListJobs")

	if conn == nil {
		return nil, errors.New("Connection cann't be nil.")
	}

	k := keys.Jobs()
	jobs, err := conn.Children(k)
	if err != nil {
		log.Printf("ERROR ListJobs: %s", err.Error())
		return nil, err
	}

	result := make([]*model.JobRecord, len(jobs))

	for i, j := range jobs {
		jr, err := GetJob(conn, j)
		if err != nil {
			return nil, err
		}

		if jr.GetID() == "" {
			jr.SetID(j)
			conn.SetRecordForPath(keys.Job(j), &jr.Record)
		}

		result[i] = jr
	}

	return result, nil
}
