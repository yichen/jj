package cluster

import (
	"testing"
	"time"

	"bitbucket.org/yichen/jj/cluster/controller"
	"bitbucket.org/yichen/jj/cluster/participant"
)

func TestClusterManagerIntegration(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	clusterName := "TestClusterManagerIntegration"
	c := controller.NewController(clusterName, "localhost:2181")
	err := c.Connect()
	if err != nil {
		t.FailNow()
	}
	defer c.Disconnect()

	// wait for the controller to set up cluster (zookeeper path)
	time.Sleep(100 * time.Millisecond)

	p1 := participant.New("localhost:2181", clusterName, "localhost_11111")
	err = p1.Connect()
	if err != nil {
		t.FailNow()
	}
	defer p1.Disconnect()

	p2 := participant.New("localhost:2181", clusterName, "localhost_22222")
	err = p2.Connect()
	if err != nil {
		t.FailNow()
	}
	defer p2.Disconnect()

	time.Sleep(100 * time.Millisecond)

	li := c.LiveInstances()
	if len(li) != 2 {
		t.FailNow()
	}

	if li[0] != "localhost_11111" && li[0] != "localhost_22222" {
		t.FailNow()
	}
	if li[1] != "localhost_11111" && li[1] != "localhost_22222" {
		t.FailNow()
	}
	if li[0] == li[1] {
		t.FailNow()
	}

}
