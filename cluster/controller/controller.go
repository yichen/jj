package controller

import (
	"log"
	"time"

	"bitbucket.org/yichen/jj/cluster/core"
	"bitbucket.org/yichen/jj/cluster/model"
	"bitbucket.org/yichen/jj/cluster/zk"
	"bitbucket.org/yichen/jj/cluster/zkcache"
	"bitbucket.org/yichen/jj/common"
	"github.com/aws/aws-sdk-go-v2/aws/external"
	"github.com/aws/aws-sdk-go-v2/service/emr"
	"github.com/docker/libkv/store"
	"github.com/docker/libkv/store/zookeeper"
	"github.com/lafikl/consistent"
)

type controllerState uint8

const (
	csStarted      controllerState = 1
	csStopped      controllerState = 2
	csDisconnected controllerState = 3
)

type LiveInstanceChangeListener func(liveInstances []string) error

type Controller struct {
	clusterName  string
	instanceName string
	instanceType core.InstanceType
	zkAddress    string
	keys         core.KeyBuilder

	zkConn                   *core.Connection
	zkstore                  *store.Store
	queue                    *zkcache.Cache
	queueNotify              chan struct{}
	liveinstances            *zkcache.Cache
	curLiveInstancesSnapshot []string
	liveinstancesNotify      chan struct{}
	stopCh                   chan struct{}
	hashing                  *consistent.Consistent

	// status
	state controllerState
}

func NewController(clusterName string, zkAddress string) *Controller {
	zookeeper.Register()

	return &Controller{
		clusterName:         clusterName,
		instanceName:        "CONTROLLER",
		instanceType:        core.CONTROLLER,
		zkAddress:           zkAddress,
		queueNotify:         make(chan struct{}, 100),
		liveinstancesNotify: make(chan struct{}, 100),
		stopCh:              make(chan struct{}),
		hashing:             consistent.New(),
		keys:                core.KeyBuilder{clusterName},
	}
}

func (c *Controller) Connect() error {
	log.Printf("BEGIN Controller.Connect for cluster: %s, zk: %s\n", c.clusterName, c.zkAddress)
	defer log.Printf("END Controller.Connect for cluster: %s, zk: %s\n", c.clusterName, c.zkAddress)

	zkconn := core.NewConnection(c.zkAddress)

	done, err := zkconn.IsClusterSetup(c.clusterName)
	if err != nil {
		log.Printf("ERROR Controller.Connect: %s", err.Error())
		return err
	}
	if !done {
		err = zkconn.SetupCluster(c.clusterName)
		if err != nil {
			log.Printf("ERROR Controller.Connect: %s", err.Error())
			return err
		}
	}
	c.zkConn = zkconn

	go c.loop()

	err = c.watchLiveInstances()
	if err != nil {
		log.Printf("ERROR Controller.Connect: %s. Stopping Controller", err.Error())
		c.stopCh <- struct{}{}
		return err
	}

	return nil
}

func (c *Controller) IsConnected() bool {
	return c.state == csStarted
}

func (c *Controller) Disconnect() {
	log.Printf("BEGIN Controller.Disconnect for cluster: %s, zk: %s\n", c.clusterName, c.zkAddress)
	defer log.Printf("END Controller.Disconnect for cluster: %s, zk: %s\n", c.clusterName, c.zkAddress)

	if c.stopCh != nil {
		c.stopCh <- struct{}{}
		close(c.stopCh)
		c.stopCh = nil
	}

	if c.zkConn != nil {
		c.zkConn.Disconnect()
		c.zkConn = nil
	}

	c.state = csDisconnected
}

func (c *Controller) loop() {

	log.Printf("BEGIN controller event loop. cluster: %s\n", c.clusterName)
	c.state = csStarted

	// keep a record of the previous live instances list so we only act on the change
	// when there is a difference
	liveInstances := common.NewLinkedSet()

	tickerC := time.NewTicker(20 * time.Second).C

	TenSecondTickerC := time.NewTicker(10 * time.Second).C

	for {
		select {

		case <-c.liveinstancesNotify:
			currentLiveInstances := c.liveinstances.GetAll()
			curli := common.NewLinkedSet()
			for k := range currentLiveInstances {
				curli.Push(k)
			}
			if liveInstances.Equals(curli) {
				log.Printf("no live instance change detected. current list: [%s]\n",
					liveInstances.String())
				continue
			}

			log.Printf("detected live instances changed from [%s] to [%s]\n",
				liveInstances.String(), curli.String())

			liveInstances = curli
			c.hashing = consistent.New()
			for k := range currentLiveInstances {
				c.hashing.Add(k)
			}

			if len(currentLiveInstances) > 0 {
				err := c.watchJobQueue()
				if err != nil {
					return
				}
			}

		case <-TenSecondTickerC:
			//TODO: instead of querying zk, watch the tree instead
			jobs, err := c.zkConn.Children(c.keys.Jobs())
			if err != nil {
				log.Printf("ERROR Controller.loop: %s\n", err.Error())
				continue
			}

			for _, j := range jobs {
				r, err := c.zkConn.GetRecordFromPath(c.keys.Job(j))
				if err != nil {
					log.Printf("ERROR Controller.loop: failed to get record from zk: %s", err.Error())
					continue
				}

				jr := model.JobRecord{Record: *r}
				state := jr.GetState()
				if state != "COMPLETED" && state != "FAILED" && state != "ERROR" {
					cluster := jr.GetCluster()
					step := jr.GetStep()

					if cluster == "" || step == "" {
						log.Printf("WARN: job [%s] has invalid information. Both cluster and step cannot be empty", j)
						props := make(map[string]string)
						props["STATE"] = "ERROR"
						zk.UpdateJob(c.zkConn, j, props)
						continue
					}

					cfg, err := external.LoadDefaultAWSConfig()
					if err != nil {
						log.Printf("ERROR Controller.loop: %s", err.Error())
						continue
					}

					client := emr.New(cfg)
					input := emr.DescribeStepInput{
						ClusterId: &cluster,
						StepId:    &step,
					}
					request := client.DescribeStepRequest(&input)
					response, err := request.Send()
					if err != nil {
						log.Printf("ERROR Controller.loop: %s", err.Error())
						continue
					}

					currentState := string(response.Step.Status.State)
					if currentState != state {
						props := make(map[string]string)
						props["STATE"] = currentState
						err = zk.UpdateJob(c.zkConn, j, props)
						if err != nil {
							log.Printf("ERROR UpdateJob: %s", err.Error())
							continue
						}

						log.Printf("INFO Controller.loop. STATE for job [%s] is changed from [%s] to [%s]", j, state, currentState)
					}
				}
			}

		case <-c.queueNotify:
			c.processGlobalJobQueue()

		case <-tickerC:
			// at a regular interval, do the following:
			// [1] scan the global job queue and process each item to make sure the real-time process does not
			//     miss anything
			// [2] todo
			c.processGlobalJobQueue()

		case <-c.stopCh:
			log.Printf("END controller event loop. cluster: %s, zk: %s\n", c.clusterName, c.zkAddress)
			c.state = csStopped
			return
		}
	}
}

func (c *Controller) processGlobalJobQueue() {

}

func (c *Controller) watchLiveInstances() error {
	li, err := zkcache.New(c.zkAddress, c.keys.LiveInstances())
	if err != nil {
		return err
	}
	li.Connect(c.liveinstancesNotify)
	c.liveinstances = li
	return nil
}

func (c *Controller) watchJobQueue() error {
	return nil
}

func (c *Controller) LiveInstances() []string {
	m := c.liveinstances.GetAll()

	var result []string

	for k := range m {
		result = append(result, k)
	}

	return result
}
