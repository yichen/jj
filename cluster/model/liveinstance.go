package model

import (
	"fmt"
	"os"

	"bitbucket.org/yichen/jj/cluster/core"
)

type LiveInstance struct {
	core.Record
}

// NewLiveInstanceNode creates a new instance of Record for representing
// a live instance.
func NewLiveInstanceNode(participantID string, sessionID string) *core.Record {
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	node := core.NewRecord(participantID)
	node.SetSimpleField("SESSION_ID", sessionID)
	node.SetSimpleField("LIVE_INSTANCE", fmt.Sprintf("%d@%s", os.Getpid(), hostname))

	return node
}
