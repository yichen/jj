package model

import (
	"bitbucket.org/yichen/jj/cluster/core"
)

type JobRecord struct {
	core.Record
}

func NewJobRecord(cluster string) JobRecord {
	r := JobRecord{}
	r.Record.SetSimpleField("CLUSTER", cluster)
	return r
}

func (r JobRecord) GetCluster() string {
	return r.GetSimpleField("CLUSTER").(string)
}

func (r JobRecord) GetStep() string {
	v := r.GetSimpleField("STEP")
	if v == nil {
		return ""
	}
	return v.(string)
}

func (r *JobRecord) SetStep(stepID string) {
	r.SetSimpleField("STEP", stepID)
}

func (r *JobRecord) SetID(jobID string) {
	r.SetSimpleField("ID", jobID)
}

func (r JobRecord) GetID() string {
	v := r.GetSimpleField("ID")
	if v == nil {
		return ""
	}
	return v.(string)
}

func (r *JobRecord) SetState(state string) {
	r.SetSimpleField("STATE", state)
}

func (r JobRecord) GetState() string {
	v := r.GetSimpleField("STATE")
	if v == nil {
		return ""
	}
	return v.(string)
}

func (r JobRecord) GetLogStatus(name string) string {
	return r.GetMapField("LOG_STATUS", name)
}

func (r *JobRecord) SetLogStatus(name string, status string) {
	r.SetMapField("LOG_STATUS", name, status)
}
