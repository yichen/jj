package zkcache

import (
	"path"
	"strings"
	"time"

	"github.com/docker/libkv"
	"github.com/docker/libkv/store"
	"github.com/docker/libkv/store/zookeeper"
	"github.com/sasha-s/go-deadlock"
)

type CacheUpdateListener func(prev map[string]string, current map[string]string)

const (
	CacheStatusConnected    = "connected"
	CacheStatusDisconnected = "disconnected"
)

// Cache is a zookeeper backed cache. It watches zookeer or etcd tree
// and update the content automatically.
type Cache struct {
	lock       deadlock.RWMutex
	store      store.Store
	keypath    string
	stopChan   chan struct{}
	updateChan <-chan []*store.KVPair
	container  map[string]string
	notifyChan chan<- struct{}
	listeners  []CacheUpdateListener
	status     string
}

// New creates a new Zookeeper backed cache
func New(zkAddress string, key string) (*Cache, error) {
	zookeeper.Register()

	config := store.Config{
		ConnectionTimeout: 30 * time.Second,
	}

	conn := strings.Split(zkAddress, ",")
	zkstore, err := libkv.NewStore(store.ZK, conn, &config)
	if err != nil {
		return nil, err
	}

	return NewCache(zkstore, key)
}

func NewCache(s store.Store, key string) (*Cache, error) {
	key = TrimPath(key)

	c := Cache{
		store:     s,
		keypath:   key,
		stopChan:  make(chan struct{}, 1),
		container: make(map[string]string),
	}

	exists, err := c.store.Exists(c.keypath)
	if err != nil {
		return nil, err
	}

	if !exists {
		c.store.Put(c.keypath, []byte(""), nil)
	}

	return &c, nil
}

func (c *Cache) RegisterListener(listener CacheUpdateListener) {
	c.listeners = append(c.listeners, listener)
}

func (c *Cache) Connect(notify chan<- struct{}) {
	c.notifyChan = notify
	go c.update()
	c.status = CacheStatusConnected
}

func (c *Cache) update() {
	kvch, err := c.store.WatchTree(c.keypath, c.stopChan)
	if err != nil {
		panic(err)
	}

	for kvc := range kvch {
		c.lock.Lock()
		c.container = make(map[string]string)
		for _, kv := range kvc {
			key := kv.Key
			value := string(kv.Value)
			c.container[key] = value
		}
		c.lock.Unlock()

		if c.notifyChan != nil {
			go func() {
				if len(c.notifyChan) == 0 && c.status != CacheStatusDisconnected {
					c.notifyChan <- struct{}{}
				}
			}()
		}
	}
}

func (c *Cache) Disconnect() {
	if c.stopChan != nil {
		c.stopChan <- struct{}{}
		close(c.stopChan)
	}
	c.status = CacheStatusDisconnected
}

func (c *Cache) Get(key string) (string, bool) {
	key = TrimPath(key)
	v, exists := c.container[key]
	return v, exists
}

func (c *Cache) GetAll() map[string]string {
	c.lock.RLock()
	defer c.lock.RUnlock()

	r := make(map[string]string)

	for k, v := range c.container {
		r[k] = v
	}

	return r
}

func (c *Cache) Set(key string, value string) error {
	key = TrimPath(key)
	absoluteKey := path.Join(c.keypath, key)
	return c.store.Put(absoluteKey, []byte(value), nil)
}

func (c *Cache) Len() int {
	return len(c.container)
}

func TrimPath(p string) string {
	if strings.HasPrefix(p, "/") {
		p = strings.TrimPrefix(p, "/")
	}
	return p
}
