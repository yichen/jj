package zkcache

import (
	"path"
	"testing"
	"time"

	"bitbucket.org/yichen/jj/cluster/core"
	"bitbucket.org/yichen/jj/cluster/model"
	"github.com/docker/libkv"
	"github.com/docker/libkv/store"
	"github.com/docker/libkv/store/zookeeper"
)

func init() {
	zookeeper.Register()
}

func TestCache_Connect(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	config := store.Config{
		ConnectionTimeout: 10 * time.Second,
	}
	store, err := libkv.NewStore(store.ZK, []string{"localhost:2181"}, &config)
	if err != nil {
		t.FailNow()
	}
	defer store.Close()

	cachepath := "TestCache_Connect"
	store.DeleteTree(cachepath)
	if err := store.Put(cachepath, []byte(""), nil); err != nil {
		t.Errorf("failed to create cachepath")
	}
	defer store.DeleteTree(cachepath)

	c, err := NewCache(store, cachepath)
	if err != nil {
		t.FailNow()
	}
	c.Connect(nil)

	r1 := core.NewRecord("t1")
	r1.SetSimpleField("name", "john")
	d1, err := r1.Marshal()
	if err != nil {
		t.FailNow()
	}

	if err := store.Put(path.Join(cachepath, "t1"), d1, nil); err != nil {
		t.Errorf("failed to put data to the store")
	}
	r2 := core.NewRecord("t2")
	d2, err := r2.Marshal()
	if err != nil {
		t.FailNow()
	}

	if err := store.Put(path.Join(cachepath, "t2"), d2, nil); err != nil {
		t.Errorf("failed to put data to the store")
	}
	r3 := core.NewRecord("t3")
	d3, err := r3.Marshal()
	if err != nil {
		t.FailNow()
	}

	if err := store.Put(path.Join(cachepath, "t3"), d3, nil); err != nil {
		t.Errorf("failed to put data to the store")
	}

	time.Sleep(2 * time.Second)

	if c.Len() != 3 {
		t.FailNow()
	}

	_, exists := c.Get("t1")
	if !exists {
		t.FailNow()
	}
}

func TestCache_Set(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	config := store.Config{
		ConnectionTimeout: 10 * time.Second,
	}
	store, err := libkv.NewStore(store.ZK, []string{"localhost:2181"}, &config)
	if err != nil {
		t.FailNow()
	}
	defer store.Close()

	cachepath := "cachepath_set_and_get"
	store.DeleteTree(cachepath)
	defer store.DeleteTree(cachepath)

	c, err := NewCache(store, cachepath)
	if err != nil {
		t.FailNow()
	}
	c.Connect(nil)
	defer c.Disconnect()

	r1 := core.NewRecord("t1")
	r1.SetSimpleField("name", "john")
	d1, err := r1.Marshal()
	if err != nil {
		t.FailNow()
	}

	c.Set("t1", string(d1))
	time.Sleep(100 * time.Millisecond)

	d2, exists := c.Get("t1")
	if !exists {
		t.Errorf("failed to get the key")
	}

	r2, err := core.NewRecordFromBytes([]byte(d2))
	if err != nil {
		t.FailNow()
	}

	if r2.GetSimpleField("name") != "john" {
		t.Errorf("failed to get what's set")
	}

	// now delete the key from the store
	// and validate the key in cache does not exist anymore
	if err := store.Delete("cachepath_set_and_get/t1"); err != nil {
		t.FailNow()
	}
	time.Sleep(100 * time.Millisecond)
	if _, exists := c.Get("t1"); exists {
		t.FailNow()
	}
}

// TestMultiCacheOnSameStore makes sure that we can disconnect and re-connect
// the cache as a hack to pick up all data
func TestMultiCacheOnSameStore(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	config := store.Config{
		ConnectionTimeout: 10 * time.Second,
	}
	store1, err := libkv.NewStore(store.ZK, []string{"localhost:2181"}, &config)
	if err != nil {
		t.FailNow()
	}
	defer store1.Close()

	store2, err := libkv.NewStore(store.ZK, []string{"localhost:2181"}, &config)
	if err != nil {
		t.FailNow()
	}
	defer store2.Close()

	cachepath := "TestMultiCacheOnSameStore"
	store1.DeleteTree(cachepath)
	if err := store1.Put(cachepath, []byte(""), nil); err != nil {
		t.Errorf("failed to create cachepath")
	}
	defer store1.DeleteTree(cachepath)

	c1, err := NewCache(store1, cachepath)
	if err != nil {
		t.FailNow()
	}
	c1.Connect(nil)

	c2, err := NewCache(store2, cachepath)
	if err != nil {
		t.FailNow()
	}
	c2.Connect(nil)

	// set a record from cache1
	r1 := core.NewRecord("t1")
	r1.SetSimpleField("name", "john")
	d1, err := r1.Marshal()
	if err != nil {
		t.FailNow()
	}

	c1.Set("t1", string(d1))
	time.Sleep(200 * time.Millisecond)

	d2, exists := c2.Get("t1")
	if !exists {
		t.FailNow()
	}

	r2, err := core.NewRecordFromBytes([]byte(d2))
	if err != nil {
		t.FailNow()
	}

	if r2.GetSimpleField("name") != "john" {
		t.Errorf("failed to get what's set")
	}
}

func TestCacheReconnect(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	config := store.Config{
		ConnectionTimeout: 10 * time.Second,
	}
	store1, err := libkv.NewStore(store.ZK, []string{"localhost:2181"}, &config)
	if err != nil {
		t.FailNow()
	}
	defer store1.Close()

	cachepath := "TestCacheReconnect"
	store1.DeleteTree(cachepath)
	if err := store1.Put(cachepath, []byte(""), nil); err != nil {
		t.Errorf("failed to create cachepath")
	}
	defer store1.DeleteTree(cachepath)

	c1, err := NewCache(store1, cachepath)
	if err != nil {
		t.FailNow()
	}
	c1.Connect(nil)

	// set a record from cache1
	r1 := core.NewRecord("t1")
	r1.SetSimpleField("name", "john")
	d1, err := r1.Marshal()
	if err != nil {
		t.FailNow()
	}

	c1.Set("t1", string(d1))
	time.Sleep(200 * time.Millisecond)

	// disconnect and then re-connect
	c1.Disconnect()
	c1.Connect(nil)

	d2, exists := c1.Get("t1")
	if !exists {
		t.FailNow()
	}

	r2, err := core.NewRecordFromBytes([]byte(d2))
	if err != nil {
		t.FailNow()
	}

	if r2.GetSimpleField("name") != "john" {
		t.Errorf("failed to get what's set")
	}
}

func TestCacheNotifyUpdate(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	config := store.Config{
		ConnectionTimeout: 10 * time.Second,
	}

	store1, err := libkv.NewStore(store.ZK, []string{"localhost:2181"}, &config)
	if err != nil {
		t.FailNow()
	}
	defer store1.Close()

	time.Sleep(100 * time.Millisecond)
	cachepath := "TestCacheNotifyUpdate"
	store1.DeleteTree(cachepath)
	defer store1.DeleteTree(cachepath)

	c, err := NewCache(store1, cachepath)
	if err != nil {
		t.FailNow()
	}

	notify := make(chan struct{}, 1)
	c.Connect(notify)
	defer c.Disconnect()

	for len(notify) > 0 {
		<-notify
	}

	r1 := core.NewRecord("t1")
	r1.SetSimpleField("name", "john")
	d1, err := r1.Marshal()
	if err != nil {
		t.FailNow()
	}

	err = store1.Put("TestCacheNotifyUpdate/t1", d1, nil)
	if err != nil {
		t.FailNow()
	}

	time.Sleep(100 * time.Millisecond)
	if len(notify) == 0 {
		t.Fatal("update notification not received")
	}
}

func TestCacheWithModelContent(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	kb := core.KeyBuilder{ClusterID: "TestCacheWithModelContent"}
	li := kb.LiveInstances()
	cache, err := New("localhost:2181", TrimPath(li))
	if err != nil {
		t.FailNow()
	}
	cache.Connect(nil)
	defer cache.Disconnect()

	zkconn := core.NewConnection("localhost:2181")
	err = zkconn.Connect()
	if err != nil {
		t.FailNow()
	}
	defer zkconn.Disconnect()

	lin := model.NewLiveInstanceNode("node1", "session12345")
	lip := kb.LiveInstance("node1")

	exists, err := zkconn.Exists(lip)
	if exists {
		zkconn.Delete(lip)
	}
	zkconn.CreateRecordWithPath(lip, lin)

	time.Sleep(200 * time.Millisecond)

	v, ok := cache.Get("node1")
	if !ok {
		t.FailNow()
	}

	_, err = core.NewRecordFromString(v)
	if err != nil {
		t.FailNow()
	}
}
