package participant

import (
	"encoding/json"
	"errors"
	"log"
	"sync"
	"time"

	"bitbucket.org/yichen/jj/cluster/core"
	"bitbucket.org/yichen/jj/cluster/model"
	"bitbucket.org/yichen/jj/cluster/zkcache"
	"github.com/yichen/go-zookeeper/zk"
)

type participantState uint8

const (
	psStarted      participantState = 1
	psStopped      participantState = 2
	psDisconnected participantState = 3
)

var (
	// ErrClusterNotSetup means the helix data structure in zookeeper /{CLUSTER_NAME}
	// is not correct or does not exist
	ErrClusterNotSetup = errors.New("cluster not setup")
	// ErrEnsureParticipantConfig is returned when participant configuration cannot be
	// created in zookeeper
	ErrEnsureParticipantConfig = errors.New("participant configuration could not be added")
)

// Participant is a Helix participant node
type Participant struct {
	// zk connection
	conn *core.Connection
	// zookeeper connection string
	zkAddress string

	// The cluster this participant belongs to
	ClusterID string

	// ParticipantID is the optional identifier of this participant, by default to host_port
	ParticipantID string

	// channel to receive upon start of event loop
	started chan interface{}
	// channel to receive stop participant event
	stop chan struct{}

	// status
	state participantState

	// KeyBuilder to calculate zookeeper path
	keys core.KeyBuilder

	// job queue for this participant
	localJobQueue       *zkcache.Cache
	localJobQueueNotify chan struct{}

	// pendingJobCh contains the next pending job. There is no buffer for this channel
	// so that the producer, an goroutine will block. This can guarantee that the job state
	// can be synchronized outside of the channel.
	pendingJobCh chan string

	sync.Mutex
}

func New(zkAddress string, cluster string, name string) *Participant {
	return &Participant{
		ClusterID:           cluster,
		ParticipantID:       name,
		zkAddress:           zkAddress,
		started:             make(chan interface{}),
		stop:                make(chan struct{}, 1),
		keys:                core.KeyBuilder{ClusterID: cluster},
		localJobQueueNotify: make(chan struct{}, 1),
		pendingJobCh:        make(chan string),
	}
}

// Connect the participant
func (p *Participant) Connect() error {
	log.Printf("BEGIN Participant.Connect. cluster: %s, participantID: %s\n", p.ClusterID, p.ParticipantID)
	defer log.Printf("END Participant.Connect. cluster: %s, participantID: %s\n", p.ClusterID, p.ParticipantID)

	if p.conn == nil || !p.conn.IsConnected() {
		p.conn = core.NewConnection(p.zkAddress)
		p.conn.Connect()
	}

	if ok, err := p.conn.IsClusterSetup(p.ClusterID); !ok || err != nil {
		log.Printf("Particiant.Connect WARN: Cluster is not set up, setting up now.")
		err = p.conn.SetupCluster(p.ClusterID)
		if err != nil {
			return ErrClusterNotSetup
		}
	}

	// bring this participant alive.
	p.createLiveInstance()

	// start the event loop
	p.loop()

	// watch local job queue
	//err := p.watchLocalJobQueue()
	//if err != nil {
	//	return err
	//}

	return nil
}

func (p *Participant) GetConnection() *core.Connection {
	return p.conn
}

// Disconnect the participant from Zookeeper
func (p *Participant) Disconnect() {
	log.Printf("BEGIN Participant.Disconnect. ParticipantID: %s\n", p.ParticipantID)
	defer log.Printf("END Participant.Disconnect. ParticipantID: %s\n", p.ParticipantID)

	// do i need lock here?
	if p.state == psDisconnected {
		return
	}

	// if the state is connected, it means we are not in event loop
	// if the state is started, it means we are in event loop and need to sent
	// the stop message
	// if the status is started, it means the event loop is running
	// wait for it to stop
	if p.state == psStarted {
		p.stop <- struct{}{}
		close(p.stop)
		for p.state != psStopped {
			time.Sleep(100 * time.Millisecond)
		}
	}

	println("@@@ disconnect localJobQueue")
	if p.localJobQueueNotify != nil {
		close(p.localJobQueueNotify)
	}
	p.localJobQueue.Disconnect()

	println("@@@ close pendingJobCh")
	// stop the channel where we generate tne next pending job
	close(p.pendingJobCh)

	println("@@@ delete live instance")
	// proactively delete the liveinstance node
	lipath := p.keys.LiveInstance(p.ParticipantID)
	p.conn.Delete(lipath)

	println("@@@ disconnect zk")
	if p.conn.IsConnected() {
		p.conn.Disconnect()
		p.conn = nil
	}

	p.state = psDisconnected
}

func (p *Participant) IsConnected() bool {
	return p.state == psStarted
}

// main event loop for the participant. It watches the local queue nodes for job
// assignment
func (p *Participant) loop() {

	go func() {
		p.state = psStarted

		// ticker checks the job that is running asynchronously to obtain the status
		ticker := time.NewTicker(15 * time.Second)

		for {
			select {
			case <-p.localJobQueueNotify:
				p.processLocalJobQueue()

			case <-ticker.C:
				p.processLocalJobQueue()

			case <-p.stop:
				p.state = psStopped
				return
			}
		}
	}()
}

func (p *Participant) processLocalJobQueue() {
	// we can enter this function from multiple entry points, either upon real-time notification
	// or from regular time interval. Make sure there is only one instance by locking it.
	p.Lock()
	defer p.Unlock()
}

func (p *Participant) createLiveInstance() {
	path := p.keys.LiveInstance(p.ParticipantID)

	// if the instance already exists, it is the previous one if we are restarting too fast.
	// delete that old liveinstance node anyway
	exists, _ := p.conn.Exists(path)
	if exists {
		log.Println("detected old liveinstance node. delete it now.")
		p.conn.DeleteTree(path)
	}

	node := model.NewLiveInstanceNode(p.ParticipantID, p.conn.GetSessionID())
	data, err := json.MarshalIndent(*node, "", "  ")
	flags := int32(zk.FlagEphemeral)
	acl := zk.WorldACL(zk.PermAll)

	_, err = p.conn.Create(path, data, flags, acl)
	core.Must(err)
}

// Poll implements the competing consumer pattern, it returns the next pending job that needs to be processed
// and mark the job as currently being assigned for that consumer. The other consumers will not be able to see
// this job for a given time period (10 minutes by default). If there is no pending job in the queue, return
// empty string
func (p *Participant) Poll(worker string) string {
	var jobkey string

	select {
	case <-p.pendingJobCh:

	default:
		jobkey = ""
	}

	return jobkey
}
