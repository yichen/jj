package core

import (
	"fmt"
	"testing"
	"time"
)

var (
	testZkSvr = "localhost:2181"
)

func TestEnsurePath(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	t.Parallel()

	conn := NewConnection(testZkSvr)
	err := conn.Connect()
	if err != nil {
		t.Error(err.Error())
	}
	defer conn.Disconnect()

	now := time.Now().Local()
	cluster := "TestEnsurePath-" + now.Format("20060102150405")
	p := fmt.Sprintf("/%s/a/b/c", cluster)

	err = conn.EnsurePath(p)
	if err != nil {
		t.Error(err.Error())
	}
	defer conn.DeleteTree("/" + cluster)

	exists, err := conn.Exists(p)
	if err != nil {
		t.Error(err.Error())
	}
	if !exists {
		t.Error("should exist the path:" + p)
	}

}
