package core

type InstanceType string

const (
	CONTROLLER  = InstanceType("CONTROLLER")
	PARTICIPANT = InstanceType("PARTICIPANT")
)
