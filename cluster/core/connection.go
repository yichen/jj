package core

import (
	"path"
	"strings"
	"sync"
	"time"

	"strconv"

	"github.com/cenkalti/backoff"
	"github.com/pkg/errors"
	"github.com/yichen/go-zookeeper/zk"
)

var b = backoff.NewExponentialBackOff()

func init() {
	b.InitialInterval = 10 * time.Millisecond
	b.MaxInterval = 2 * time.Second
	b.MaxElapsedTime = 1 * time.Minute
	b.Reset()
}

type Connection struct {
	zkSvr       string
	zkConn      *zk.Conn
	isConnected bool
	stat        *zk.Stat
	sync.RWMutex
}

func NewConnection(zkSvr string) *Connection {
	conn := Connection{
		zkSvr: zkSvr,
	}

	return &conn
}

func (conn *Connection) Connect() error {
	zkServers := strings.Split(strings.TrimSpace(conn.zkSvr), ",")
	zkConn, _, err := zk.Connect(zkServers, 15*time.Second)
	if err != nil {
		return err
	}

	_, _, err = zkConn.Exists("/zookeeper")
	if err != nil {
		return err
	}

	conn.isConnected = true
	conn.zkConn = zkConn

	return nil
}

func (conn *Connection) IsConnected() bool {
	if conn == nil || conn.isConnected == false {
		return false
	}

	_, _, err := conn.zkConn.Exists("/cluster")
	if err != nil {
		conn.isConnected = false
		return false
	}

	conn.isConnected = true
	return true
}

func (conn *Connection) Disconnect() {
	conn.zkConn.Close()
	conn.isConnected = false
}

func (conn *Connection) GetSessionID() string {
	str := strconv.FormatInt(conn.zkConn.SessionID, 10)
	return str
}

func (conn *Connection) CreateEmptyNode(path string) {
	flags := int32(0)
	acl := zk.WorldACL(zk.PermAll)
	_, err := conn.Create(path, []byte(""), flags, acl)
	Must(err)
}

func (conn *Connection) CreateRecordWithData(path string, data string) {
	flags := int32(0)
	acl := zk.WorldACL(zk.PermAll)

	_, err := conn.Create(path, []byte(data), flags, acl)
	Must(err)
}

func (conn *Connection) CreateRecordWithPath(p string, r *Record) (string, error) {
	parent := path.Dir(p)
	conn.EnsurePath(parent)

	flags := int32(0)
	acl := zk.WorldACL(zk.PermAll)

	data, err := r.Marshal()
	if err != nil {
		return "", err
	}

	return conn.Create(p, data, flags, acl)
}

func (conn *Connection) CreateSequentialRecordWithPath(p string, r *Record) (string, error) {
	parent := path.Dir(p)
	conn.EnsurePath(parent)

	flags := int32(zk.FlagSequence)
	acl := zk.WorldACL(zk.PermAll)
	data, err := r.Marshal()
	Must(err)
	return conn.Create(p, data, flags, acl)
}

func (conn *Connection) Exists(path string) (bool, error) {
	var result bool
	var stat *zk.Stat

	f := func() error {
		r, s, err := conn.zkConn.Exists(path)
		if err != nil {
			return err
		}
		result = r
		stat = s
		return nil
	}

	err := backoff.Retry(f, backoff.NewExponentialBackOff())
	if err != nil {
		return result, err
	}

	conn.stat = stat
	return result, err
}

func (conn *Connection) ExistsAll(paths ...string) (bool, error) {
	for _, path := range paths {
		if exists, err := conn.Exists(path); err != nil || exists == false {
			return exists, err
		}
	}

	return true, nil
}

func (conn *Connection) Get(path string) ([]byte, error) {

	var data []byte

	f := func() error {
		if conn.zkConn == nil {
			return errors.New("zk connection gone")
		}
		d, s, err := conn.zkConn.Get(path)
		if err != nil {
			return err
		}
		data = d
		conn.stat = s
		return nil
	}

	err := backoff.Retry(f, backoff.NewExponentialBackOff())
	return data, err
}

func (conn *Connection) GetW(path string) ([]byte, <-chan zk.Event, error) {
	var data []byte
	var events <-chan zk.Event

	f := func() error {
		d, s, evts, err := conn.zkConn.GetW(path)
		if err != nil {
			return err
		}
		data = d
		conn.stat = s
		events = evts
		return nil
	}

	err := backoff.Retry(f, backoff.NewExponentialBackOff())

	return data, events, err
}

func (conn *Connection) Set(path string, data []byte) error {
	_, err := conn.zkConn.Set(path, data, conn.stat.Version)
	return err
}

func (conn *Connection) Create(path string, data []byte, flags int32, acl []zk.ACL) (string, error) {
	return conn.zkConn.Create(path, data, flags, acl)
}

func (conn *Connection) Children(path string) ([]string, error) {
	var children []string

	f := func() error {
		c, s, err := conn.zkConn.Children(path)
		if err != nil {
			return err
		}
		children = c
		conn.stat = s
		return nil
	}

	err := backoff.Retry(f, backoff.NewExponentialBackOff())

	return children, err
}

func (conn *Connection) ChildrenW(path string) ([]string, <-chan zk.Event, error) {
	var children []string
	var eventChan <-chan zk.Event

	f := func() error {
		c, s, evts, err := conn.zkConn.ChildrenW(path)
		if err != nil {
			return err
		}
		children = c
		conn.stat = s
		eventChan = evts
		return nil
	}

	err := backoff.Retry(f, backoff.NewExponentialBackOff())

	return children, eventChan, err
}

// update a map field for the znode. path is the znode path. key is the top-level key in
// the MapFields, mapProperty is the inner key, and value is the. For example:
//
// mapFields":{
// "eat1-app993.stg.linkedin.com_11932,BizProfile,p31_1,SLAVE":{
//   "CURRENT_STATE":"ONLINE"
//   ,"INFO":""
// }
// if we want to set the CURRENT_STATE to ONLINE, we call
// UpdateMapField("/RELAY/INSTANCES/{instance}/CURRENT_STATE/{sessionID}/{db}", "eat1-app993.stg.linkedin.com_11932,BizProfile,p31_1,SLAVE", "CURRENT_STATE", "ONLINE")
func (conn *Connection) UpdateMapField(path string, key string, property string, value string) error {
	data, err := conn.Get(path)
	if err != nil {
		return err
	}

	// convert the result into Record
	node, err := NewRecordFromBytes(data)
	if err != nil {
		return err
	}

	// update the value
	node.SetMapField(key, property, value)

	// mashall to bytes
	data, err = node.Marshal()
	if err != nil {
		return err
	}

	// copy back to cluster
	err = conn.Set(path, data)
	return err
}

func (conn *Connection) UpdateSimpleField(path string, key string, value string) {
	// get the current node
	data, err := conn.Get(path)
	Must(err)

	// convert the result into Record
	node, err := NewRecordFromBytes(data)
	Must(err)

	// update the value
	node.SetSimpleField(key, value)

	// mashall to bytes
	data, err = node.Marshal()
	Must(err)

	// copy back to cluster
	err = conn.Set(path, data)
	Must(err)
}

func (conn *Connection) GetSimpleFieldValueByKey(path string, key string) string {
	data, err := conn.Get(path)
	Must(err)

	node, err := NewRecordFromBytes(data)
	Must(err)

	if node.SimpleFields == nil {
		return ""
	}

	v := node.GetSimpleField(key)
	if v == nil {
		return ""
	}
	return v.(string)
}

func (conn *Connection) GetSimpleFieldBool(path string, key string) bool {
	result := conn.GetSimpleFieldValueByKey(path, key)
	return strings.ToUpper(result) == "TRUE"
}

func (conn *Connection) Delete(path string) error {
	return conn.zkConn.Delete(path, -1)
}

func (conn *Connection) DeleteTree(path string) error {
	if exists, err := conn.Exists(path); !exists || err != nil {
		return err
	}

	children, err := conn.Children(path)
	if err != nil {
		return err
	}

	if len(children) == 0 {
		err := conn.zkConn.Delete(path, -1)
		return err
	}

	for _, c := range children {
		p := path + "/" + c
		e := conn.DeleteTree(p)
		if e != nil {
			return e
		}
	}

	return conn.Delete(path)
}

func (conn *Connection) RemoveMapFieldKey(path string, key string) error {
	data, err := conn.Get(path)
	if err != nil {
		return err
	}

	node, err := NewRecordFromBytes(data)
	if err != nil {
		return err
	}

	node.RemoveMapField(key)

	data, err = node.Marshal()
	if err != nil {
		return err
	}

	// save the data back to cluster
	err = conn.Set(path, data)
	return err
}

func (conn *Connection) IsClusterSetup(cluster string) (bool, error) {
	if conn.IsConnected() == false {
		if err := conn.Connect(); err != nil {
			return false, err
		}
	}

	keys := KeyBuilder{cluster}

	return conn.ExistsAll(
		keys.Cluster(),
		keys.Config(),
		keys.Instances(),
		keys.Jobs(),
		keys.LiveInstances(),
	)
}

func (conn *Connection) SetupCluster(cluster string) error {
	if conn.IsConnected() == false {
		if err := conn.Connect(); err != nil {
			return err
		}
	}

	keys := KeyBuilder{cluster}

	err := conn.EnsureAllPaths(
		keys.Cluster(),
		keys.Config(),
		keys.Instances(),
		keys.Jobs(),
		keys.LiveInstances(),
	)

	return err
}

func (conn *Connection) Sync(path string) {
	conn.zkConn.Sync(path)
}

func (conn *Connection) GetRecordFromPath(path string) (*Record, error) {

	data, err := conn.Get(path)
	if err != nil {
		return nil, err
	}
	return NewRecordFromBytes(data)
}

func (conn *Connection) SetRecordForPath(path string, r *Record) error {

	if exists, _ := conn.Exists(path); !exists {
		conn.EnsurePath(path)
	}

	data, err := r.Marshal()
	if err != nil {
		return err
	}

	// need to get the stat.version before calling set
	conn.Lock()

	if _, err := conn.Get(path); err != nil {
		conn.Unlock()
		return err
	}

	modifiedAt := time.Now().Unix()

	r.SetSimpleField("MODIFIEDAT", modifiedAt)

	if err := conn.Set(path, data); err != nil {
		conn.Unlock()
		return err
	}

	conn.Unlock()
	return nil

}

// EnsurePath makes sure the specified path exists.
// If not, create it
func (conn *Connection) EnsureAllPaths(ps ...string) error {
	for _, p := range ps {
		err := conn.EnsurePath(p)
		if err != nil {
			return err
		}
	}

	return nil
}

func (conn *Connection) EnsurePath(p string) error {
	if strings.HasSuffix(p, "/") {
		p = strings.TrimSuffix(p, "/")
	}

	if !strings.HasPrefix(p, "/") {
		p = path.Join("/", p)
	}

	if exists, _ := conn.Exists(p); exists {
		return nil
	}

	parent := path.Dir(p)

	if exists, _ := conn.Exists(parent); !exists {
		conn.EnsurePath(parent)
	}

	conn.CreateEmptyNode(p)
	return nil
}

func Must(err error) {
	if err != nil {
		panic(err)
	}
}
