package core

import (
	"fmt"
)

// KeyBuilder generates a key path for key-value stores
type KeyBuilder struct {
	ClusterID string
}

func (k KeyBuilder) Cluster() string {
	return fmt.Sprintf("/%s", k.ClusterID)
}

func (k KeyBuilder) Election() string {
	return fmt.Sprintf("/%s/election", k.ClusterID)
}

func (k KeyBuilder) Config() string {
	return fmt.Sprintf("/%s/config", k.ClusterID)
}

func (k KeyBuilder) Instances() string {
	return fmt.Sprintf("/%s/instances", k.ClusterID)
}

func (k KeyBuilder) Instance(name string) string {
	return fmt.Sprintf("/%s/instances/%s", k.ClusterID, name)
}

func (k KeyBuilder) LiveInstances() string {
	return fmt.Sprintf("/%s/liveinstances", k.ClusterID)
}

func (k KeyBuilder) LiveInstance(name string) string {
	return fmt.Sprintf("/%s/liveinstances/%s", k.ClusterID, name)
}

func (k *KeyBuilder) ParticipantConfigs() string {
	return fmt.Sprintf("/%s/CONFIGS/PARTICIPANT", k.ClusterID)
}

func (k *KeyBuilder) ParticipantConfig(participantID string) string {
	return fmt.Sprintf("/%s/CONFIGS/PARTICIPANT/%s", k.ClusterID, participantID)
}

func (k *KeyBuilder) ClusterConfig() string {
	return fmt.Sprintf("/%s/CONFIGS/CLUSTER/%s", k.ClusterID, k.ClusterID)
}

func (k *KeyBuilder) CurrentStates(participantID string) string {
	return fmt.Sprintf("/%s/instances/%s/CURRENTSTATES", k.ClusterID, participantID)
}

func (k *KeyBuilder) CurrentStatesForSession(participantID string, sessionID string) string {
	return fmt.Sprintf("/%s/instances/%s/CURRENTSTATES/%s", k.ClusterID, participantID, sessionID)
}

func (k *KeyBuilder) CurrentStateForResource(participantID string, sessionID string, resourceID string) string {
	return fmt.Sprintf("/%s/instances/%s/CURRENTSTATES/%s/%s", k.ClusterID, participantID, sessionID, resourceID)
}

func (k *KeyBuilder) Jobs() string {
	return fmt.Sprintf("/%s/jobs", k.ClusterID)
}

func (k KeyBuilder) Job(jobID string) string {
	return fmt.Sprintf("/%s/jobs/%s", k.ClusterID, jobID)
}
