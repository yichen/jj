package container

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"bitbucket.org/yichen/jj/common"
)

type ContainerLog struct {
	ExitCode string
	history  common.History
}

func FromText(text string) (chan string, error) {
	lines := strings.Split(text, "\n")
	lch := make(chan string, 1000)

	go func() {
		for _, l := range lines {
			lch <- l
		}
		close(lch)
	}()

	return lch, nil
}

func FromFile(fn string) (chan string, error) {
	f, err := os.Open(fn)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	lch := make(chan string, 1000)
	scanner := bufio.NewScanner(f)
	go func() {
		for scanner.Scan() {
			txt := scanner.Text()
			fmt.Println(txt)
			lch <- txt
		}
		close(lch)
	}()

	return lch, err
}
