package am

//go:generate peg -inline -switch yarn/am/am.peg

import (
	"fmt"
	"github.com/pointlander/peg/tree"
	"math"
	"sort"
	"strconv"
)

const endSymbol rune = 1114112

/* The rule types inferred from the grammar are below. */
type pegRule uint8

const (
	ruleUnknown pegRule = iota
	ruleamroot
	ruleamlog
	ruleattempt
	rulefinalstatus
	ruleattemptId
	ruleexitCode
	ruleappStatus
	ruleamexecutor
	rulenumExecutor
	rulenumCores
	ruleintVal
	ruleoomgc
	ruledd
	rulets
)

var rul3s = [...]string{
	"Unknown",
	"amroot",
	"amlog",
	"attempt",
	"finalstatus",
	"attemptId",
	"exitCode",
	"appStatus",
	"amexecutor",
	"numExecutor",
	"numCores",
	"intVal",
	"oomgc",
	"dd",
	"ts",
}

type token32 struct {
	pegRule
	begin, end uint32
}

func (t *token32) String() string {
	return fmt.Sprintf("\x1B[34m%v\x1B[m %v %v", rul3s[t.pegRule], t.begin, t.end)
}

type node32 struct {
	token32
	up, next *node32
}

func (node *node32) print(pretty bool, buffer string) {
	var print func(node *node32, depth int)
	print = func(node *node32, depth int) {
		for node != nil {
			for c := 0; c < depth; c++ {
				fmt.Printf(" ")
			}
			rule := rul3s[node.pegRule]
			quote := strconv.Quote(string(([]rune(buffer)[node.begin:node.end])))
			if !pretty {
				fmt.Printf("%v %v\n", rule, quote)
			} else {
				fmt.Printf("\x1B[34m%v\x1B[m %v\n", rule, quote)
			}
			if node.up != nil {
				print(node.up, depth+1)
			}
			node = node.next
		}
	}
	print(node, 0)
}

func (node *node32) Print(buffer string) {
	node.print(false, buffer)
}

func (node *node32) PrettyPrint(buffer string) {
	node.print(true, buffer)
}

type tokens32 struct {
	tree []token32
}

func (t *tokens32) Trim(length uint32) {
	t.tree = t.tree[:length]
}

func (t *tokens32) Print() {
	for _, token := range t.tree {
		fmt.Println(token.String())
	}
}

func (t *tokens32) AST() *node32 {
	type element struct {
		node *node32
		down *element
	}
	tokens := t.Tokens()
	var stack *element
	for _, token := range tokens {
		if token.begin == token.end {
			continue
		}
		node := &node32{token32: token}
		for stack != nil && stack.node.begin >= token.begin && stack.node.end <= token.end {
			stack.node.next = node.up
			node.up = stack.node
			stack = stack.down
		}
		stack = &element{node: node, down: stack}
	}
	if stack != nil {
		return stack.node
	}
	return nil
}

func (t *tokens32) PrintSyntaxTree(buffer string) {
	t.AST().Print(buffer)
}

func (t *tokens32) PrettyPrintSyntaxTree(buffer string) {
	t.AST().PrettyPrint(buffer)
}

func (t *tokens32) Add(rule pegRule, begin, end, index uint32) {
	if tree := t.tree; int(index) >= len(tree) {
		expanded := make([]token32, 2*len(tree))
		copy(expanded, tree)
		t.tree = expanded
	}
	t.tree[index] = token32{
		pegRule: rule,
		begin:   begin,
		end:     end,
	}
}

func (t *tokens32) Tokens() []token32 {
	return t.tree
}

type Peg struct {
	*tree.Tree

	Buffer string
	buffer []rune
	rules  [15]func() bool
	parse  func(rule ...int) error
	reset  func()
	Pretty bool
	tokens32
}

func (p *Peg) Parse(rule ...int) error {
	return p.parse(rule...)
}

func (p *Peg) Reset() {
	p.reset()
}

type textPosition struct {
	line, symbol int
}

type textPositionMap map[int]textPosition

func translatePositions(buffer []rune, positions []int) textPositionMap {
	length, translations, j, line, symbol := len(positions), make(textPositionMap, len(positions)), 0, 1, 0
	sort.Ints(positions)

search:
	for i, c := range buffer {
		if c == '\n' {
			line, symbol = line+1, 0
		} else {
			symbol++
		}
		if i == positions[j] {
			translations[positions[j]] = textPosition{line, symbol}
			for j++; j < length; j++ {
				if i != positions[j] {
					continue search
				}
			}
			break search
		}
	}

	return translations
}

type parseError struct {
	p   *Peg
	max token32
}

func (e *parseError) Error() string {
	tokens, error := []token32{e.max}, "\n"
	positions, p := make([]int, 2*len(tokens)), 0
	for _, token := range tokens {
		positions[p], p = int(token.begin), p+1
		positions[p], p = int(token.end), p+1
	}
	translations := translatePositions(e.p.buffer, positions)
	format := "parse error near %v (line %v symbol %v - line %v symbol %v):\n%v\n"
	if e.p.Pretty {
		format = "parse error near \x1B[34m%v\x1B[m (line %v symbol %v - line %v symbol %v):\n%v\n"
	}
	for _, token := range tokens {
		begin, end := int(token.begin), int(token.end)
		error += fmt.Sprintf(format,
			rul3s[token.pegRule],
			translations[begin].line, translations[begin].symbol,
			translations[end].line, translations[end].symbol,
			strconv.Quote(string(e.p.buffer[begin:end])))
	}

	return error
}

func (p *Peg) PrintSyntaxTree() {
	if p.Pretty {
		p.tokens32.PrettyPrintSyntaxTree(p.Buffer)
	} else {
		p.tokens32.PrintSyntaxTree(p.Buffer)
	}
}

func (p *Peg) Init() {
	var (
		max                  token32
		position, tokenIndex uint32
		buffer               []rune
	)
	p.reset = func() {
		max = token32{}
		position, tokenIndex = 0, 0

		p.buffer = []rune(p.Buffer)
		if len(p.buffer) == 0 || p.buffer[len(p.buffer)-1] != endSymbol {
			p.buffer = append(p.buffer, endSymbol)
		}
		buffer = p.buffer
	}
	p.reset()

	_rules := p.rules
	tree := tokens32{tree: make([]token32, math.MaxInt16)}
	p.parse = func(rule ...int) error {
		r := 1
		if len(rule) > 0 {
			r = rule[0]
		}
		matches := p.rules[r]()
		p.tokens32 = tree
		if matches {
			p.Trim(tokenIndex)
			return nil
		}
		return &parseError{p, max}
	}

	add := func(rule pegRule, begin uint32) {
		tree.Add(rule, begin, position, tokenIndex)
		tokenIndex++
		if begin != position && position > max.end {
			max = token32{rule, begin, position}
		}
	}

	matchDot := func() bool {
		if buffer[position] != endSymbol {
			position++
			return true
		}
		return false
	}

	/*matchChar := func(c byte) bool {
		if buffer[position] == c {
			position++
			return true
		}
		return false
	}*/

	/*matchRange := func(lower byte, upper byte) bool {
		if c := buffer[position]; c >= lower && c <= upper {
			position++
			return true
		}
		return false
	}*/

	_rules = [...]func() bool{
		nil,
		/* 0 amroot <- <((amlog / amexecutor / oomgc / .*) !.)> */
		func() bool {
			position0, tokenIndex0 := position, tokenIndex
			{
				position1 := position
				{
					position2, tokenIndex2 := position, tokenIndex
					{
						position4 := position
						if !_rules[rulets]() {
							goto l3
						}
						if buffer[position] != rune(' ') {
							goto l3
						}
						position++
					l5:
						{
							position6, tokenIndex6 := position, tokenIndex
							if buffer[position] != rune(' ') {
								goto l6
							}
							position++
							goto l5
						l6:
							position, tokenIndex = position6, tokenIndex6
						}
						if buffer[position] != rune('I') {
							goto l3
						}
						position++
						if buffer[position] != rune('N') {
							goto l3
						}
						position++
						if buffer[position] != rune('F') {
							goto l3
						}
						position++
						if buffer[position] != rune('O') {
							goto l3
						}
						position++
						if buffer[position] != rune(' ') {
							goto l3
						}
						position++
						if buffer[position] != rune('A') {
							goto l3
						}
						position++
						if buffer[position] != rune('p') {
							goto l3
						}
						position++
						if buffer[position] != rune('p') {
							goto l3
						}
						position++
						if buffer[position] != rune('l') {
							goto l3
						}
						position++
						if buffer[position] != rune('i') {
							goto l3
						}
						position++
						if buffer[position] != rune('c') {
							goto l3
						}
						position++
						if buffer[position] != rune('a') {
							goto l3
						}
						position++
						if buffer[position] != rune('t') {
							goto l3
						}
						position++
						if buffer[position] != rune('i') {
							goto l3
						}
						position++
						if buffer[position] != rune('o') {
							goto l3
						}
						position++
						if buffer[position] != rune('n') {
							goto l3
						}
						position++
						if buffer[position] != rune('M') {
							goto l3
						}
						position++
						if buffer[position] != rune('a') {
							goto l3
						}
						position++
						if buffer[position] != rune('s') {
							goto l3
						}
						position++
						if buffer[position] != rune('t') {
							goto l3
						}
						position++
						if buffer[position] != rune('e') {
							goto l3
						}
						position++
						if buffer[position] != rune('r') {
							goto l3
						}
						position++
						if buffer[position] != rune(':') {
							goto l3
						}
						position++
						if buffer[position] != rune(' ') {
							goto l3
						}
						position++
					l7:
						{
							position8, tokenIndex8 := position, tokenIndex
							if buffer[position] != rune(' ') {
								goto l8
							}
							position++
							goto l7
						l8:
							position, tokenIndex = position8, tokenIndex8
						}
						{
							position9, tokenIndex9 := position, tokenIndex
							{
								position11 := position
								if buffer[position] != rune('A') {
									goto l10
								}
								position++
								if buffer[position] != rune('p') {
									goto l10
								}
								position++
								if buffer[position] != rune('p') {
									goto l10
								}
								position++
								if buffer[position] != rune('l') {
									goto l10
								}
								position++
								if buffer[position] != rune('i') {
									goto l10
								}
								position++
								if buffer[position] != rune('c') {
									goto l10
								}
								position++
								if buffer[position] != rune('a') {
									goto l10
								}
								position++
								if buffer[position] != rune('t') {
									goto l10
								}
								position++
								if buffer[position] != rune('i') {
									goto l10
								}
								position++
								if buffer[position] != rune('o') {
									goto l10
								}
								position++
								if buffer[position] != rune('n') {
									goto l10
								}
								position++
								if buffer[position] != rune('A') {
									goto l10
								}
								position++
								if buffer[position] != rune('t') {
									goto l10
								}
								position++
								if buffer[position] != rune('t') {
									goto l10
								}
								position++
								if buffer[position] != rune('e') {
									goto l10
								}
								position++
								if buffer[position] != rune('m') {
									goto l10
								}
								position++
								if buffer[position] != rune('p') {
									goto l10
								}
								position++
								if buffer[position] != rune('t') {
									goto l10
								}
								position++
								if buffer[position] != rune('I') {
									goto l10
								}
								position++
								if buffer[position] != rune('d') {
									goto l10
								}
								position++
								if buffer[position] != rune(':') {
									goto l10
								}
								position++
								if buffer[position] != rune(' ') {
									goto l10
								}
								position++
							l12:
								{
									position13, tokenIndex13 := position, tokenIndex
									if buffer[position] != rune(' ') {
										goto l13
									}
									position++
									goto l12
								l13:
									position, tokenIndex = position13, tokenIndex13
								}
								{
									position14 := position
									{
										switch buffer[position] {
										case '_':
											if buffer[position] != rune('_') {
												goto l10
											}
											position++
											break
										case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
											if c := buffer[position]; c < rune('0') || c > rune('9') {
												goto l10
											}
											position++
											break
										case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
											if c := buffer[position]; c < rune('A') || c > rune('Z') {
												goto l10
											}
											position++
											break
										default:
											if c := buffer[position]; c < rune('a') || c > rune('z') {
												goto l10
											}
											position++
											break
										}
									}

								l15:
									{
										position16, tokenIndex16 := position, tokenIndex
										{
											switch buffer[position] {
											case '_':
												if buffer[position] != rune('_') {
													goto l16
												}
												position++
												break
											case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
												if c := buffer[position]; c < rune('0') || c > rune('9') {
													goto l16
												}
												position++
												break
											case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
												if c := buffer[position]; c < rune('A') || c > rune('Z') {
													goto l16
												}
												position++
												break
											default:
												if c := buffer[position]; c < rune('a') || c > rune('z') {
													goto l16
												}
												position++
												break
											}
										}

										goto l15
									l16:
										position, tokenIndex = position16, tokenIndex16
									}
									add(ruleattemptId, position14)
								}
								add(ruleattempt, position11)
							}
							goto l9
						l10:
							position, tokenIndex = position9, tokenIndex9
							{
								position19 := position
								if buffer[position] != rune('F') {
									goto l3
								}
								position++
								if buffer[position] != rune('i') {
									goto l3
								}
								position++
								if buffer[position] != rune('n') {
									goto l3
								}
								position++
								if buffer[position] != rune('a') {
									goto l3
								}
								position++
								if buffer[position] != rune('l') {
									goto l3
								}
								position++
								if buffer[position] != rune(' ') {
									goto l3
								}
								position++
								if buffer[position] != rune('a') {
									goto l3
								}
								position++
								if buffer[position] != rune('p') {
									goto l3
								}
								position++
								if buffer[position] != rune('p') {
									goto l3
								}
								position++
								if buffer[position] != rune(' ') {
									goto l3
								}
								position++
								if buffer[position] != rune('S') {
									goto l3
								}
								position++
								if buffer[position] != rune('t') {
									goto l3
								}
								position++
								if buffer[position] != rune('a') {
									goto l3
								}
								position++
								if buffer[position] != rune('t') {
									goto l3
								}
								position++
								if buffer[position] != rune('u') {
									goto l3
								}
								position++
								if buffer[position] != rune('s') {
									goto l3
								}
								position++
								if buffer[position] != rune(':') {
									goto l3
								}
								position++
								if buffer[position] != rune(' ') {
									goto l3
								}
								position++
								{
									position20 := position
								l21:
									{
										position22, tokenIndex22 := position, tokenIndex
										if c := buffer[position]; c < rune('A') || c > rune('Z') {
											goto l22
										}
										position++
										goto l21
									l22:
										position, tokenIndex = position22, tokenIndex22
									}
									add(ruleappStatus, position20)
								}
								if buffer[position] != rune(',') {
									goto l3
								}
								position++
								if buffer[position] != rune(' ') {
									goto l3
								}
								position++
								if buffer[position] != rune('E') {
									goto l3
								}
								position++
								if buffer[position] != rune('x') {
									goto l3
								}
								position++
								if buffer[position] != rune('i') {
									goto l3
								}
								position++
								if buffer[position] != rune('t') {
									goto l3
								}
								position++
								if buffer[position] != rune('C') {
									goto l3
								}
								position++
								if buffer[position] != rune('o') {
									goto l3
								}
								position++
								if buffer[position] != rune('d') {
									goto l3
								}
								position++
								if buffer[position] != rune('e') {
									goto l3
								}
								position++
								if buffer[position] != rune(':') {
									goto l3
								}
								position++
								if buffer[position] != rune(' ') {
									goto l3
								}
								position++
								{
									position23 := position
									if !_rules[ruleintVal]() {
										goto l3
									}
									add(ruleexitCode, position23)
								}
								add(rulefinalstatus, position19)
							}
						}
					l9:
						add(ruleamlog, position4)
					}
					goto l2
				l3:
					position, tokenIndex = position2, tokenIndex2
					{
						position25 := position
						if !_rules[rulets]() {
							goto l24
						}
						if buffer[position] != rune(' ') {
							goto l24
						}
						position++
					l26:
						{
							position27, tokenIndex27 := position, tokenIndex
							if buffer[position] != rune(' ') {
								goto l27
							}
							position++
							goto l26
						l27:
							position, tokenIndex = position27, tokenIndex27
						}
						if buffer[position] != rune('I') {
							goto l24
						}
						position++
						if buffer[position] != rune('N') {
							goto l24
						}
						position++
						if buffer[position] != rune('F') {
							goto l24
						}
						position++
						if buffer[position] != rune('O') {
							goto l24
						}
						position++
						if buffer[position] != rune(' ') {
							goto l24
						}
						position++
						if buffer[position] != rune('Y') {
							goto l24
						}
						position++
						if buffer[position] != rune('a') {
							goto l24
						}
						position++
						if buffer[position] != rune('r') {
							goto l24
						}
						position++
						if buffer[position] != rune('n') {
							goto l24
						}
						position++
						if buffer[position] != rune('A') {
							goto l24
						}
						position++
						if buffer[position] != rune('l') {
							goto l24
						}
						position++
						if buffer[position] != rune('l') {
							goto l24
						}
						position++
						if buffer[position] != rune('o') {
							goto l24
						}
						position++
						if buffer[position] != rune('c') {
							goto l24
						}
						position++
						if buffer[position] != rune('a') {
							goto l24
						}
						position++
						if buffer[position] != rune('t') {
							goto l24
						}
						position++
						if buffer[position] != rune('o') {
							goto l24
						}
						position++
						if buffer[position] != rune('r') {
							goto l24
						}
						position++
						if buffer[position] != rune(':') {
							goto l24
						}
						position++
						if buffer[position] != rune(' ') {
							goto l24
						}
						position++
						if buffer[position] != rune('W') {
							goto l24
						}
						position++
						if buffer[position] != rune('i') {
							goto l24
						}
						position++
						if buffer[position] != rune('l') {
							goto l24
						}
						position++
						if buffer[position] != rune('l') {
							goto l24
						}
						position++
						if buffer[position] != rune(' ') {
							goto l24
						}
						position++
						if buffer[position] != rune('r') {
							goto l24
						}
						position++
						if buffer[position] != rune('e') {
							goto l24
						}
						position++
						if buffer[position] != rune('q') {
							goto l24
						}
						position++
						if buffer[position] != rune('u') {
							goto l24
						}
						position++
						if buffer[position] != rune('e') {
							goto l24
						}
						position++
						if buffer[position] != rune('s') {
							goto l24
						}
						position++
						if buffer[position] != rune('t') {
							goto l24
						}
						position++
						if buffer[position] != rune(' ') {
							goto l24
						}
						position++
						{
							position28 := position
							if !_rules[ruleintVal]() {
								goto l24
							}
							add(rulenumExecutor, position28)
						}
						if buffer[position] != rune(' ') {
							goto l24
						}
						position++
						if buffer[position] != rune('e') {
							goto l24
						}
						position++
						if buffer[position] != rune('x') {
							goto l24
						}
						position++
						if buffer[position] != rune('e') {
							goto l24
						}
						position++
						if buffer[position] != rune('c') {
							goto l24
						}
						position++
						if buffer[position] != rune('u') {
							goto l24
						}
						position++
						if buffer[position] != rune('t') {
							goto l24
						}
						position++
						if buffer[position] != rune('o') {
							goto l24
						}
						position++
						if buffer[position] != rune('r') {
							goto l24
						}
						position++
						if buffer[position] != rune(' ') {
							goto l24
						}
						position++
						if buffer[position] != rune('c') {
							goto l24
						}
						position++
						if buffer[position] != rune('o') {
							goto l24
						}
						position++
						if buffer[position] != rune('n') {
							goto l24
						}
						position++
						if buffer[position] != rune('t') {
							goto l24
						}
						position++
						if buffer[position] != rune('a') {
							goto l24
						}
						position++
						if buffer[position] != rune('i') {
							goto l24
						}
						position++
						if buffer[position] != rune('n') {
							goto l24
						}
						position++
						if buffer[position] != rune('e') {
							goto l24
						}
						position++
						if buffer[position] != rune('r') {
							goto l24
						}
						position++
						if buffer[position] != rune('s') {
							goto l24
						}
						position++
						if buffer[position] != rune(',') {
							goto l24
						}
						position++
						if buffer[position] != rune(' ') {
							goto l24
						}
						position++
						if buffer[position] != rune('e') {
							goto l24
						}
						position++
						if buffer[position] != rune('a') {
							goto l24
						}
						position++
						if buffer[position] != rune('c') {
							goto l24
						}
						position++
						if buffer[position] != rune('h') {
							goto l24
						}
						position++
						if buffer[position] != rune(' ') {
							goto l24
						}
						position++
						if buffer[position] != rune('w') {
							goto l24
						}
						position++
						if buffer[position] != rune('i') {
							goto l24
						}
						position++
						if buffer[position] != rune('t') {
							goto l24
						}
						position++
						if buffer[position] != rune('h') {
							goto l24
						}
						position++
						if buffer[position] != rune(' ') {
							goto l24
						}
						position++
						{
							position29 := position
							if !_rules[ruleintVal]() {
								goto l24
							}
							add(rulenumCores, position29)
						}
						if buffer[position] != rune(' ') {
							goto l24
						}
						position++
						if buffer[position] != rune('c') {
							goto l24
						}
						position++
						if buffer[position] != rune('o') {
							goto l24
						}
						position++
						if buffer[position] != rune('r') {
							goto l24
						}
						position++
						if buffer[position] != rune('e') {
							goto l24
						}
						position++
						if buffer[position] != rune('s') {
							goto l24
						}
						position++
						if buffer[position] != rune(' ') {
							goto l24
						}
						position++
						if buffer[position] != rune('a') {
							goto l24
						}
						position++
						if buffer[position] != rune('n') {
							goto l24
						}
						position++
						if buffer[position] != rune('d') {
							goto l24
						}
						position++
						if buffer[position] != rune(' ') {
							goto l24
						}
						position++
					l30:
						{
							position31, tokenIndex31 := position, tokenIndex
							if !matchDot() {
								goto l31
							}
							goto l30
						l31:
							position, tokenIndex = position31, tokenIndex31
						}
						add(ruleamexecutor, position25)
					}
					goto l2
				l24:
					position, tokenIndex = position2, tokenIndex2
					{
						position33 := position
						if !_rules[rulets]() {
							goto l32
						}
						if buffer[position] != rune(' ') {
							goto l32
						}
						position++
						if buffer[position] != rune('E') {
							goto l32
						}
						position++
						if buffer[position] != rune('R') {
							goto l32
						}
						position++
						if buffer[position] != rune('R') {
							goto l32
						}
						position++
						if buffer[position] != rune('O') {
							goto l32
						}
						position++
						if buffer[position] != rune('R') {
							goto l32
						}
						position++
						if buffer[position] != rune(' ') {
							goto l32
						}
						position++
						if buffer[position] != rune('e') {
							goto l32
						}
						position++
						if buffer[position] != rune('x') {
							goto l32
						}
						position++
						if buffer[position] != rune('e') {
							goto l32
						}
						position++
						if buffer[position] != rune('c') {
							goto l32
						}
						position++
						if buffer[position] != rune('u') {
							goto l32
						}
						position++
						if buffer[position] != rune('t') {
							goto l32
						}
						position++
						if buffer[position] != rune('o') {
							goto l32
						}
						position++
						if buffer[position] != rune('r') {
							goto l32
						}
						position++
						if buffer[position] != rune('.') {
							goto l32
						}
						position++
						if buffer[position] != rune('E') {
							goto l32
						}
						position++
						if buffer[position] != rune('x') {
							goto l32
						}
						position++
						if buffer[position] != rune('e') {
							goto l32
						}
						position++
						if buffer[position] != rune('c') {
							goto l32
						}
						position++
						if buffer[position] != rune('u') {
							goto l32
						}
						position++
						if buffer[position] != rune('t') {
							goto l32
						}
						position++
						if buffer[position] != rune('o') {
							goto l32
						}
						position++
						if buffer[position] != rune('r') {
							goto l32
						}
						position++
						if buffer[position] != rune(':') {
							goto l32
						}
						position++
						if buffer[position] != rune(' ') {
							goto l32
						}
						position++
						if buffer[position] != rune('E') {
							goto l32
						}
						position++
						if buffer[position] != rune('x') {
							goto l32
						}
						position++
						if buffer[position] != rune('c') {
							goto l32
						}
						position++
						if buffer[position] != rune('e') {
							goto l32
						}
						position++
						if buffer[position] != rune('p') {
							goto l32
						}
						position++
						if buffer[position] != rune('t') {
							goto l32
						}
						position++
						if buffer[position] != rune('i') {
							goto l32
						}
						position++
						if buffer[position] != rune('o') {
							goto l32
						}
						position++
						if buffer[position] != rune('n') {
							goto l32
						}
						position++
						if buffer[position] != rune(' ') {
							goto l32
						}
						position++
						if buffer[position] != rune('i') {
							goto l32
						}
						position++
						if buffer[position] != rune('n') {
							goto l32
						}
						position++
						if buffer[position] != rune(' ') {
							goto l32
						}
						position++
						if buffer[position] != rune('t') {
							goto l32
						}
						position++
						if buffer[position] != rune('a') {
							goto l32
						}
						position++
						if buffer[position] != rune('s') {
							goto l32
						}
						position++
						if buffer[position] != rune('k') {
							goto l32
						}
						position++
						if buffer[position] != rune(' ') {
							goto l32
						}
						position++
						{
							position36, tokenIndex36 := position, tokenIndex
							if buffer[position] != rune('\'') {
								goto l37
							}
							position++
							goto l36
						l37:
							position, tokenIndex = position36, tokenIndex36
							if buffer[position] != rune('\'') {
								goto l38
							}
							position++
							goto l36
						l38:
							position, tokenIndex = position36, tokenIndex36
							if buffer[position] != rune('\'') {
								goto l39
							}
							position++
							goto l36
						l39:
							position, tokenIndex = position36, tokenIndex36
							if buffer[position] != rune('\'') {
								goto l40
							}
							position++
							goto l36
						l40:
							position, tokenIndex = position36, tokenIndex36
							if buffer[position] != rune('\'') {
								goto l41
							}
							position++
							goto l36
						l41:
							position, tokenIndex = position36, tokenIndex36
							if buffer[position] != rune('\'') {
								goto l42
							}
							position++
							goto l36
						l42:
							position, tokenIndex = position36, tokenIndex36
							if buffer[position] != rune('\'') {
								goto l43
							}
							position++
							goto l36
						l43:
							position, tokenIndex = position36, tokenIndex36
							{
								switch buffer[position] {
								case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
									if c := buffer[position]; c < rune('0') || c > rune('9') {
										goto l32
									}
									position++
									break
								case '\'':
									if buffer[position] != rune('\'') {
										goto l32
									}
									position++
									break
								case ')':
									if buffer[position] != rune(')') {
										goto l32
									}
									position++
									break
								case '(':
									if buffer[position] != rune('(') {
										goto l32
									}
									position++
									break
								case '.':
									if buffer[position] != rune('.') {
										goto l32
									}
									position++
									break
								case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
									if c := buffer[position]; c < rune('A') || c > rune('Z') {
										goto l32
									}
									position++
									break
								case ' ':
									if buffer[position] != rune(' ') {
										goto l32
									}
									position++
									break
								default:
									if c := buffer[position]; c < rune('a') || c > rune('z') {
										goto l32
									}
									position++
									break
								}
							}

						}
					l36:
					l34:
						{
							position35, tokenIndex35 := position, tokenIndex
							{
								position45, tokenIndex45 := position, tokenIndex
								if buffer[position] != rune('\'') {
									goto l46
								}
								position++
								goto l45
							l46:
								position, tokenIndex = position45, tokenIndex45
								if buffer[position] != rune('\'') {
									goto l47
								}
								position++
								goto l45
							l47:
								position, tokenIndex = position45, tokenIndex45
								if buffer[position] != rune('\'') {
									goto l48
								}
								position++
								goto l45
							l48:
								position, tokenIndex = position45, tokenIndex45
								if buffer[position] != rune('\'') {
									goto l49
								}
								position++
								goto l45
							l49:
								position, tokenIndex = position45, tokenIndex45
								if buffer[position] != rune('\'') {
									goto l50
								}
								position++
								goto l45
							l50:
								position, tokenIndex = position45, tokenIndex45
								if buffer[position] != rune('\'') {
									goto l51
								}
								position++
								goto l45
							l51:
								position, tokenIndex = position45, tokenIndex45
								if buffer[position] != rune('\'') {
									goto l52
								}
								position++
								goto l45
							l52:
								position, tokenIndex = position45, tokenIndex45
								{
									switch buffer[position] {
									case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
										if c := buffer[position]; c < rune('0') || c > rune('9') {
											goto l35
										}
										position++
										break
									case '\'':
										if buffer[position] != rune('\'') {
											goto l35
										}
										position++
										break
									case ')':
										if buffer[position] != rune(')') {
											goto l35
										}
										position++
										break
									case '(':
										if buffer[position] != rune('(') {
											goto l35
										}
										position++
										break
									case '.':
										if buffer[position] != rune('.') {
											goto l35
										}
										position++
										break
									case 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z':
										if c := buffer[position]; c < rune('A') || c > rune('Z') {
											goto l35
										}
										position++
										break
									case ' ':
										if buffer[position] != rune(' ') {
											goto l35
										}
										position++
										break
									default:
										if c := buffer[position]; c < rune('a') || c > rune('z') {
											goto l35
										}
										position++
										break
									}
								}

							}
						l45:
							goto l34
						l35:
							position, tokenIndex = position35, tokenIndex35
						}
						if buffer[position] != rune('\n') {
							goto l32
						}
						position++
						if buffer[position] != rune('j') {
							goto l32
						}
						position++
						if buffer[position] != rune('a') {
							goto l32
						}
						position++
						if buffer[position] != rune('v') {
							goto l32
						}
						position++
						if buffer[position] != rune('a') {
							goto l32
						}
						position++
						if buffer[position] != rune('.') {
							goto l32
						}
						position++
						if buffer[position] != rune('l') {
							goto l32
						}
						position++
						if buffer[position] != rune('a') {
							goto l32
						}
						position++
						if buffer[position] != rune('n') {
							goto l32
						}
						position++
						if buffer[position] != rune('g') {
							goto l32
						}
						position++
						if buffer[position] != rune('.') {
							goto l32
						}
						position++
						if buffer[position] != rune('O') {
							goto l32
						}
						position++
						if buffer[position] != rune('u') {
							goto l32
						}
						position++
						if buffer[position] != rune('t') {
							goto l32
						}
						position++
						if buffer[position] != rune('O') {
							goto l32
						}
						position++
						if buffer[position] != rune('f') {
							goto l32
						}
						position++
						if buffer[position] != rune('M') {
							goto l32
						}
						position++
						if buffer[position] != rune('e') {
							goto l32
						}
						position++
						if buffer[position] != rune('m') {
							goto l32
						}
						position++
						if buffer[position] != rune('o') {
							goto l32
						}
						position++
						if buffer[position] != rune('r') {
							goto l32
						}
						position++
						if buffer[position] != rune('y') {
							goto l32
						}
						position++
						if buffer[position] != rune('E') {
							goto l32
						}
						position++
						if buffer[position] != rune('r') {
							goto l32
						}
						position++
						if buffer[position] != rune('r') {
							goto l32
						}
						position++
						if buffer[position] != rune('o') {
							goto l32
						}
						position++
						if buffer[position] != rune('r') {
							goto l32
						}
						position++
						if buffer[position] != rune(':') {
							goto l32
						}
						position++
						if buffer[position] != rune(' ') {
							goto l32
						}
						position++
						if buffer[position] != rune('G') {
							goto l32
						}
						position++
						if buffer[position] != rune('C') {
							goto l32
						}
						position++
						if buffer[position] != rune(' ') {
							goto l32
						}
						position++
						if buffer[position] != rune('o') {
							goto l32
						}
						position++
						if buffer[position] != rune('v') {
							goto l32
						}
						position++
						if buffer[position] != rune('e') {
							goto l32
						}
						position++
						if buffer[position] != rune('r') {
							goto l32
						}
						position++
						if buffer[position] != rune('h') {
							goto l32
						}
						position++
						if buffer[position] != rune('e') {
							goto l32
						}
						position++
						if buffer[position] != rune('a') {
							goto l32
						}
						position++
						if buffer[position] != rune('d') {
							goto l32
						}
						position++
						if buffer[position] != rune(' ') {
							goto l32
						}
						position++
						if buffer[position] != rune('l') {
							goto l32
						}
						position++
						if buffer[position] != rune('i') {
							goto l32
						}
						position++
						if buffer[position] != rune('m') {
							goto l32
						}
						position++
						if buffer[position] != rune('i') {
							goto l32
						}
						position++
						if buffer[position] != rune('t') {
							goto l32
						}
						position++
						if buffer[position] != rune(' ') {
							goto l32
						}
						position++
						if buffer[position] != rune('e') {
							goto l32
						}
						position++
						if buffer[position] != rune('x') {
							goto l32
						}
						position++
						if buffer[position] != rune('c') {
							goto l32
						}
						position++
						if buffer[position] != rune('e') {
							goto l32
						}
						position++
						if buffer[position] != rune('e') {
							goto l32
						}
						position++
						if buffer[position] != rune('d') {
							goto l32
						}
						position++
						if buffer[position] != rune('e') {
							goto l32
						}
						position++
						if buffer[position] != rune('d') {
							goto l32
						}
						position++
					l54:
						{
							position55, tokenIndex55 := position, tokenIndex
							if !matchDot() {
								goto l55
							}
							goto l54
						l55:
							position, tokenIndex = position55, tokenIndex55
						}
						add(ruleoomgc, position33)
					}
					goto l2
				l32:
					position, tokenIndex = position2, tokenIndex2
				l56:
					{
						position57, tokenIndex57 := position, tokenIndex
						if !matchDot() {
							goto l57
						}
						goto l56
					l57:
						position, tokenIndex = position57, tokenIndex57
					}
				}
			l2:
				{
					position58, tokenIndex58 := position, tokenIndex
					if !matchDot() {
						goto l58
					}
					goto l0
				l58:
					position, tokenIndex = position58, tokenIndex58
				}
				add(ruleamroot, position1)
			}
			return true
		l0:
			position, tokenIndex = position0, tokenIndex0
			return false
		},
		/* 1 amlog <- <(ts ' '+ ('I' 'N' 'F' 'O' ' ' 'A' 'p' 'p' 'l' 'i' 'c' 'a' 't' 'i' 'o' 'n' 'M' 'a' 's' 't' 'e' 'r' ':') ' '+ (attempt / finalstatus))> */
		nil,
		/* 2 attempt <- <('A' 'p' 'p' 'l' 'i' 'c' 'a' 't' 'i' 'o' 'n' 'A' 't' 't' 'e' 'm' 'p' 't' 'I' 'd' ':' ' '+ attemptId)> */
		nil,
		/* 3 finalstatus <- <('F' 'i' 'n' 'a' 'l' ' ' 'a' 'p' 'p' ' ' 'S' 't' 'a' 't' 'u' 's' ':' ' ' appStatus (',' ' ' 'E' 'x' 'i' 't' 'C' 'o' 'd' 'e' ':' ' ') exitCode)> */
		nil,
		/* 4 attemptId <- <((&('_') '_') | (&('0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9') [0-9]) | (&('A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z') [A-Z]) | (&('a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z') [a-z]))+> */
		nil,
		/* 5 exitCode <- <intVal> */
		nil,
		/* 6 appStatus <- <[A-Z]*> */
		nil,
		/* 7 amexecutor <- <(ts ' '+ ('I' 'N' 'F' 'O' ' ' 'Y' 'a' 'r' 'n' 'A' 'l' 'l' 'o' 'c' 'a' 't' 'o' 'r' ':' ' ' 'W' 'i' 'l' 'l' ' ' 'r' 'e' 'q' 'u' 'e' 's' 't' ' ') numExecutor (' ' 'e' 'x' 'e' 'c' 'u' 't' 'o' 'r' ' ' 'c' 'o' 'n' 't' 'a' 'i' 'n' 'e' 'r' 's' ',' ' ' 'e' 'a' 'c' 'h' ' ' 'w' 'i' 't' 'h' ' ') numCores (' ' 'c' 'o' 'r' 'e' 's' ' ' 'a' 'n' 'd' ' ') .*)> */
		nil,
		/* 8 numExecutor <- <intVal> */
		nil,
		/* 9 numCores <- <intVal> */
		nil,
		/* 10 intVal <- <[0-9]+> */
		func() bool {
			position68, tokenIndex68 := position, tokenIndex
			{
				position69 := position
				if c := buffer[position]; c < rune('0') || c > rune('9') {
					goto l68
				}
				position++
			l70:
				{
					position71, tokenIndex71 := position, tokenIndex
					if c := buffer[position]; c < rune('0') || c > rune('9') {
						goto l71
					}
					position++
					goto l70
				l71:
					position, tokenIndex = position71, tokenIndex71
				}
				add(ruleintVal, position69)
			}
			return true
		l68:
			position, tokenIndex = position68, tokenIndex68
			return false
		},
		/* 11 oomgc <- <(ts (' ' 'E' 'R' 'R' 'O' 'R' ' ' 'e' 'x' 'e' 'c' 'u' 't' 'o' 'r' '.' 'E' 'x' 'e' 'c' 'u' 't' 'o' 'r' ':' ' ' 'E' 'x' 'c' 'e' 'p' 't' 'i' 'o' 'n' ' ' 'i' 'n' ' ' 't' 'a' 's' 'k' ' ') ('\'' / '\'' / '\'' / '\'' / '\'' / '\'' / '\'' / ((&('0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9') [0-9]) | (&('\'') '\'') | (&(')') ')') | (&('(') '(') | (&('.') '.') | (&('A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z') [A-Z]) | (&(' ') ' ') | (&('a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z') [a-z])))+ ('\n' 'j' 'a' 'v' 'a' '.' 'l' 'a' 'n' 'g' '.' 'O' 'u' 't' 'O' 'f' 'M' 'e' 'm' 'o' 'r' 'y' 'E' 'r' 'r' 'o' 'r' ':' ' ' 'G' 'C' ' ' 'o' 'v' 'e' 'r' 'h' 'e' 'a' 'd' ' ' 'l' 'i' 'm' 'i' 't' ' ' 'e' 'x' 'c' 'e' 'e' 'd' 'e' 'd') .*)> */
		nil,
		/* 12 dd <- <([0-9] [0-9])> */
		func() bool {
			position73, tokenIndex73 := position, tokenIndex
			{
				position74 := position
				if c := buffer[position]; c < rune('0') || c > rune('9') {
					goto l73
				}
				position++
				if c := buffer[position]; c < rune('0') || c > rune('9') {
					goto l73
				}
				position++
				add(ruledd, position74)
			}
			return true
		l73:
			position, tokenIndex = position73, tokenIndex73
			return false
		},
		/* 13 ts <- <(dd '/' dd '/' dd ' ' dd ':' dd ':' dd)> */
		func() bool {
			position75, tokenIndex75 := position, tokenIndex
			{
				position76 := position
				if !_rules[ruledd]() {
					goto l75
				}
				if buffer[position] != rune('/') {
					goto l75
				}
				position++
				if !_rules[ruledd]() {
					goto l75
				}
				if buffer[position] != rune('/') {
					goto l75
				}
				position++
				if !_rules[ruledd]() {
					goto l75
				}
				if buffer[position] != rune(' ') {
					goto l75
				}
				position++
				if !_rules[ruledd]() {
					goto l75
				}
				if buffer[position] != rune(':') {
					goto l75
				}
				position++
				if !_rules[ruledd]() {
					goto l75
				}
				if buffer[position] != rune(':') {
					goto l75
				}
				position++
				if !_rules[ruledd]() {
					goto l75
				}
				add(rulets, position76)
			}
			return true
		l75:
			position, tokenIndex = position75, tokenIndex75
			return false
		},
	}
	p.rules = _rules
}
