package am

import "testing"

func TestApplicationMasterLog_Analyze(t *testing.T) {
	logs := `18/09/04 21:00:35 INFO ApplicationMaster: Registered signal handlers for [TERM, HUP, INT]
18/09/04 21:00:36 INFO ApplicationMaster: ApplicationAttemptId: appattempt_1534927092706_59201_000001
18/09/04 21:00:36 INFO ApplicationMaster: Waiting for Spark driver to be reachable.
18/09/04 21:00:37 INFO ApplicationMaster: Driver now available: 172.21.161.128:58771
18/09/04 21:00:37 INFO ApplicationMaster$AMEndpoint: Add WebUI Filter. AddWebUIFilter(org.apache.hadoop.yarn.server.webproxy.amfilter.AmIpFilter,Map(PROXY_HOSTS -> i-094a810c67fe0eabf.inst.aws.us-east-1.prod.musta.ch,i-b52e1215.inst.aws.us-east-1.prod.musta.ch, PROXY_URI_BASES -> http://i-094a810c67fe0eabf.inst.aws.us-east-1.prod.musta.ch:8088/proxy/application_1534927092706_59201,http://i-b52e1215.inst.aws.us-east-1.prod.musta.ch:8088/proxy/application_1534927092706_59201),/proxy/application_1534927092706_59201)
18/09/04 21:00:37 INFO YarnRMClient: Registering the ApplicationMaster
18/09/04 21:00:37 INFO ApplicationMaster: Started progress reporter thread with (heartbeat : 3000, initial allocation : 200) intervals
18/09/05 03:00:27 INFO ApplicationMaster: Final app Status: FAILED, ExitCode: 16
18/09/05 03:00:27 ERROR ApplicationMaster: RECEIVED SIGNAL 15: SIGTERM`

	am := ApplicationMasterLog{}
	err := am.ProcessText(logs)
	if err != nil {
		t.FailNow()
	}
}

func TestAMLog_FinalAppStatus(t *testing.T) {
	l := "18/09/05 03:00:27 INFO ApplicationMaster: Final app Status: FAILED, ExitCode: 16"
	//l := "18/09/04 21:00:36 INFO ApplicationMaster: ApplicationAttemptId: appattempt_1534927092706_59201_000001"

	am := ApplicationMasterLog{}
	err := am.ProcessText(l)
	if err != nil {
		t.FailNow()
	}

	if am.Status != "FAILED" {
		t.FailNow()
	}

	if am.ExitCode != "16" {
		t.FailNow()
	}
}

func TestLaunchExecutors(t *testing.T) {
	l := "18/09/04 21:00:37 INFO YarnAllocator: Will request 500 executor containers, each with 2 cores and 16384 MB memory including 4096 MB overhead"
	am := ApplicationMasterLog{}
	err := am.ProcessText(l)
	if err != nil {
		t.FailNow()
	}

	if am.NumExecutors != "500" {
		t.FailNow()
	}
}
