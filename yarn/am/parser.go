package am

import (
	"bufio"
	"fmt"
	"log"
	"os/exec"
	"strings"
	"time"

	"github.com/pointlander/peg/tree"
)

type ApplicationMasterLog struct {
	StartAt      time.Time
	EndAt        time.Time
	NumExecutors string
	Status       string
	ExitCode     string
}

func FromText(text string) (chan string, error) {
	lines := strings.Split(text, "\n")
	lch := make(chan string, 1000)

	go func() {
		for _, l := range lines {
			lch <- l
		}
		close(lch)
	}()

	return lch, nil
}

func FromFile(fn string) (chan string, error) {
	// grep "ApplicationMaster" logfile
	cmd := exec.Command("/bin/bash", "-c", "grep ApplicationMaster "+fn)
	reader, err := cmd.StdoutPipe()

	if err != nil {
		return nil, err
	}

	lch := make(chan string, 1000)

	scanner := bufio.NewScanner(reader)
	go func() {
		for scanner.Scan() {
			txt := scanner.Text()
			fmt.Println(txt)
			lch <- txt
		}
		close(lch)
	}()

	err = cmd.Start()
	if err != nil {
		return nil, err
	}

	err = cmd.Wait()
	return lch, err
}

func (am *ApplicationMasterLog) Analyze(l chan string) {
	for line := range l {
		am.parse(line)
	}
}

func (am *ApplicationMasterLog) ProcessText(text string) error {
	lch, err := FromText(text)
	if err != nil {
		return err
	}

	am.Analyze(lch)

	//var wg sync.WaitGroup
	//for i := 1; i <= 2; i++ {
	//	wg.Add(1)
	//	go func() {
	//		defer wg.Done()
	//		am.Analyze(lch)
	//	}()
	//}
	//wg.Wait()

	return nil
}

func (am *ApplicationMasterLog) ProcessFile(fn string) error {
	lch, err := FromFile(fn)
	if err != nil {
		return err
	}

	am.Analyze(lch)

	//var wg sync.WaitGroup
	//for i := 1; i <= 10; i++ {
	//	wg.Add(1)
	//	go func() {
	//		defer wg.Done()
	//		am.Analyze(lch)
	//	}()
	//}
	//wg.Wait()

	return nil
}

func (am *ApplicationMasterLog) parse(line string) error {
	p := &Peg{Tree: tree.New(true, true, false), Buffer: string(line)}
	p.Init()
	p.Reset()
	err := p.Parse()
	if err != nil {
		log.Fatal(err.Error())
		return err
	}

	tokens := p.Tokens()

	firstWillRequestExeuctorContainers := false

	for _, t := range tokens {
		if t.pegRule == ruleappStatus {
			am.Status = line[t.begin:t.end]
		}

		if t.pegRule == ruleexitCode {
			am.ExitCode = line[t.begin:t.end]
		}

		if t.pegRule == rulenumExecutor && !firstWillRequestExeuctorContainers {
			am.NumExecutors = line[t.begin:t.end]
			firstWillRequestExeuctorContainers = true
		}

	}

	return nil
}
