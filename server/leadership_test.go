package server

import (
	"testing"
	"time"
)

func TestNewLeadership(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	t.Parallel()

	host1isLeader := false
	host1becomesLeader := LeaderElectionCallback(func() error {
		host1isLeader = true
		return nil
	})

	host1becomesFollower := LeaderElectionCallback(func() error {
		host1isLeader = false
		return nil
	})

	l := NewLeadership("localhost:2181", "election", "host1", host1becomesLeader, host1becomesFollower)
	go l.JoinElection()
	defer l.Stop()

	time.Sleep(time.Duration(1 * time.Second))
	if !host1isLeader {
		t.FailNow()
	}

	host2isLeader := false
	host2becomesLeader := LeaderElectionCallback(func() error {
		host2isLeader = true
		return nil
	})

	host2becomesFollower := LeaderElectionCallback(func() error {
		host2isLeader = false
		return nil
	})

	l2 := NewLeadership("localhost:2181", "election", "host2", host2becomesLeader, host2becomesFollower)
	go l2.JoinElection()

	time.Sleep(time.Duration(1 * time.Second))

	if host1isLeader && host2isLeader || !host1isLeader && !host2isLeader {
		t.FailNow()
	}

	l.Resign()
	time.Sleep(time.Duration(1 * time.Second))

	if host1isLeader || !host2isLeader {
		t.FailNow()
	}
}
