package server

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"path"
	"time"

	"bitbucket.org/yichen/jj/cluster/controller"
	"bitbucket.org/yichen/jj/cluster/participant"
	"bitbucket.org/yichen/jj/cluster/zk"
	"bitbucket.org/yichen/jj/common"
	"bitbucket.org/yichen/jj/pb"
	"github.com/aws/aws-sdk-go-v2/aws/endpoints"
	"github.com/aws/aws-sdk-go-v2/aws/external"
	"github.com/aws/aws-sdk-go-v2/service/emr"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type Server struct {
	zkAddr           string
	cluster          string
	name             string
	port             int
	httpPort         int
	controller       *controller.Controller
	participant      *participant.Participant
	leadership       *Leadership
	onBecomeLeader   LeaderElectionCallback
	onBecomeFollower LeaderElectionCallback

	proxy *http.Server

	stopCh chan struct{}
}

func NewServer(zkAddr string, cluster string, name string, port int, httpPort int) Server {
	s := Server{
		zkAddr:   zkAddr,
		cluster:  cluster,
		name:     name,
		port:     port,
		httpPort: httpPort,
		stopCh:   make(chan struct{}),
	}

	s.onBecomeLeader = s.BecomeLeader
	s.onBecomeFollower = s.BecomeFollower

	return s
}

func (s *Server) Start() error {

	// start leader election
	s.startLeaderElection()

	// wait for the leader election to happen and the controller is elected hopefully by now.
	time.Sleep(200 * time.Millisecond)

	// start participant
	err := s.startParticipant()
	if err != nil {
		return err
	}

	// start gRPC API server
	go s.runGRPC()
	time.Sleep(500 * time.Millisecond)

	// start REST endpoint
	go s.runProxy()

	return err
}

func (s *Server) startLeaderElection() {
	electionPath := path.Join(s.cluster, "election")
	s.leadership = NewLeadership(s.zkAddr, electionPath, s.name, s.onBecomeLeader, s.onBecomeFollower)
	go s.leadership.JoinElection()

}

func (s *Server) startParticipant() error {
	s.participant = participant.New(s.zkAddr, s.cluster, s.name)
	return s.participant.Connect()
}

func (s *Server) runGRPC() error {
	log.Printf("BEGIN runGRPC. node: %s, port: %d\n", s.name, s.port)

	var err error

	go func() {
		lis, err := net.Listen("tcp", fmt.Sprintf(":%d", s.port))
		if err != nil {
			log.Fatalf("failed to runGRPC. Error: %s\n", err.Error())
			return
		}
		server := grpc.NewServer()
		pb.RegisterJJServiceServer(server, s)
		reflection.Register(server)
		err = server.Serve(lis)
		if err != nil {
			log.Fatalf("failed to runGRPC. Error: %s\n", err.Error())
			return
		}
	}()

	// block and keep the gRPC server running
	<-s.stopCh
	log.Printf("END runGRPC. node: %s, port: %d\n", s.name, s.port)
	return err
}

func (s *Server) runProxy() error {
	log.Printf("BEGIN runProxy. node: %s, http_port: %d\n", s.name, s.httpPort)
	defer log.Printf("END runProxy. node: %s, http_port: %d\n", s.name, s.httpPort)

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	srv := &http.Server{Addr: fmt.Sprintf(":%d", s.httpPort)}
	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	proxyEndpoint := fmt.Sprintf("localhost:%d", s.port)
	err := pb.RegisterJJServiceHandlerFromEndpoint(ctx, mux, proxyEndpoint, opts)
	if err != nil {
		return err
	}

	srv.Handler = mux
	s.proxy = srv

	err = s.proxy.ListenAndServe()

	if err != nil {
		log.Printf("ERROR runProxy: %s", err.Error())
	}

	return err
}

func (s *Server) BecomeLeader() error {
	log.Printf("BEGIN BecomeLeader. node: %s\n", s.name)
	defer log.Printf("END BecomeLeader. node: %s\n", s.name)

	if s.controller == nil {
		s.controller = controller.NewController(s.cluster, s.zkAddr)
	}

	err := s.controller.Connect()
	return err
}

func (s *Server) BecomeFollower() error {
	log.Printf("BEGIN BecomeFollower. node: %s\n", s.name)
	defer log.Printf("END BecomeFollower. node: %s\n", s.name)

	if s.controller != nil && s.controller.IsConnected() {
		s.controller.Disconnect()
	}

	s.controller = nil

	return nil
}

func (s *Server) Stop() {
	log.Printf("BEGIN Master.Stop. node: %s\n", s.name)
	defer log.Printf("END Master.Stop. node: %s\n", s.name)

	// shut down REST gateway
	log.Print("Shutting done REST gateway...")
	err := s.proxy.Shutdown(context.Background())
	if err != nil {
		log.Printf("Shutting done REST gateway...%s", err.Error())
	} else {
		log.Print("Shutting done REST gateway...SUCCESS")
	}

	// stop gRPC server
	log.Print("Shutting done gRPC service...")
	s.stopCh <- struct{}{}
	close(s.stopCh)
	log.Print("Shutting done gRPC service...DONE")
	time.Sleep(1 * time.Second)
}

func (s *Server) Health(ctx context.Context, req *pb.Empty) (*pb.HealthResponse, error) {
	resp := pb.HealthResponse{
		Health:  "OK",
		Version: common.Version,
	}
	return &resp, nil
}

func (s *Server) GetLog(req *pb.GetLogRequest, stream pb.JJService_GetLogServer) error {
	log.Printf("BEGIN GetLog: job: %s\n", req.Job)
	defer log.Printf("END GetLog: job: %s\n", req.Job)

	size := 100000
	batchContent := ""
	batchLimit := 1000
	batchSize := 0
	batchCounter := 0
	for i := 0; i < size; i++ {
		content := fmt.Sprintf("Sample log line, %d/%d", i, size)
		batchContent += "\n" + content
		batchSize++

		if batchSize < batchLimit {
			continue
		}

		batchCounter++
		println("@@@@ next batch", batchCounter)

		l := pb.Log{Content: batchContent}
		err := stream.Send(&l)
		if err != nil {
			log.Printf("ERROR GetLog: stream.Send: %s\n", err.Error())
			return err
		}
		batchContent = ""
		batchSize = 0
	}

	if batchContent != "" {
		l := pb.Log{Content: batchContent}
		err := stream.Send(&l)
		if err != nil {
			log.Printf("ERROR GetLog: stream.Send: %s\n", err.Error())
			return err
		}
	}

	//jr, err := zk.GetJob(s.participant.GetConnection(), req.Job)
	//if err != nil {
	//	return err
	//}
	//
	//log.Printf("INFO GetLog: job: %s, JobRecord: %v\n", req.Job, jr)

	//cluster := jr.GetCluster()
	//step := jr.GetStep()
	//
	//err = common.DownloadStepLogs(req.Job, cluster, step)
	//if err != nil {
	//	log.Printf("ERROR GetLog: unable to download step logs for job %s", req.Job)
	//	return err
	//}
	//
	//logC := common.CatStepLogs(req.Job)
	//for ll := range logC {
	//	l := pb.Log{Content: ll}
	//	err = stream.Send(&l)
	//	if err != nil {
	//		log.Printf("ERROR GetLog: stream.Send: %s\n", err.Error())
	//		return err
	//	}
	//}

	return nil
}

func (s *Server) ListClusters(ctx context.Context, req *pb.ListClustersRequest) (*pb.ListClustersResponse, error) {
	log.Printf("BEGIN ListClusters")
	defer log.Printf("END ListCluster")

	resp := pb.ListClustersResponse{}

	cfg, err := external.LoadDefaultAWSConfig()
	if err != nil {
		log.Printf("ListClusters: %s", err.Error())
		return &resp, err
	}
	cfg.Region = endpoints.UsEast1RegionID

	c := emr.New(cfg)
	input := emr.ListClustersInput{}

	if req.Active {
		input.ClusterStates = []emr.ClusterState{
			emr.ClusterStateRunning,
			emr.ClusterStateWaiting,
			emr.ClusterStateBootstrapping,
			emr.ClusterStateStarting,
			emr.ClusterStateTerminating,
		}
	} else if req.Terminated {
		input.ClusterStates = []emr.ClusterState{emr.ClusterStateTerminated}
	} else if req.Failed {
		input.ClusterStates = []emr.ClusterState{emr.ClusterStateTerminatedWithErrors}
	}

	request := c.ListClustersRequest(&input)
	response, err := request.Send()
	if err != nil {
		log.Printf("ListClusters: %s", err.Error())
		return nil, err
	}

	resp.Clusters = make([]*pb.Cluster, len(response.Clusters))

	log.Printf("ListClusters: %d clusters\n", len(resp.Clusters))

	for i, cl := range response.Clusters {
		resp.Clusters[i] = &pb.Cluster{
			Id:                      *cl.Id,
			Name:                    *cl.Name,
			NormalizedInstanceHours: *cl.NormalizedInstanceHours,
			Status:                  (*cl.Status).String(),
		}
	}

	return &resp, nil
}

func (s *Server) CreateCluster(ctx context.Context, req *pb.CreateClusterRequest) (*pb.CreateClusterResponse, error) {
	log.Printf("BEGIN CreateCluster")
	defer log.Printf("END CreateCluster")

	cfg, err := external.LoadDefaultAWSConfig()
	if err != nil {
		log.Printf("ERROR CreateCluster: %s\n", err.Error())
		return nil, err
	}

	amiVersion := "aws-5.11.1"
	hadoopApp := "Hadoop"
	sparkApp := "Spark"

	c := emr.New(cfg)
	input := emr.RunJobFlowInput{
		Name:         &req.Name,
		AmiVersion:   &amiVersion,
		Applications: []emr.Application{{Name: &hadoopApp}, {Name: &sparkApp}},
	}

	request := c.RunJobFlowRequest(&input)
	response, err := request.Send()
	if err != nil {
		log.Printf("CreateCluster: %s", err.Error())
		return nil, err
	}

	resp := pb.CreateClusterResponse{
		ID: *response.JobFlowId,
	}
	return &resp, nil
}

func (s *Server) SparkSubmit(req *pb.SparkSubmitRequest, stream pb.JJService_SparkSubmitServer) error {
	log.Printf("BEGIN SparkSubmit. Cluster: %s, class: %s, jar: %s\n", req.Cluster, req.Class, req.Jar)
	defer log.Printf("END SparkSubmit. Cluster: %s, class: %s, jar: %s\n", req.Cluster, req.Class, req.Jar)

	//
	// create a unique jobID
	//
	jobID, err := zk.CreateJob(s.participant.GetConnection(), req.Cluster)
	if err != nil {
		log.Printf("ERROR SparkSubmit: failed to create a job ID: %s\n", err.Error())
		return err
	}
	log.Printf("SparkSubmit: JobID: %s\n", jobID)

	cfg, err := external.LoadDefaultAWSConfig()
	if err != nil {
		log.Printf("ERROR SparkSubmit: %s\n", err.Error())
		return err
	}
	cfg.Region = endpoints.UsEast1RegionID

	c := emr.New(cfg)

	name := "Spark Job from JobServer"

	jarlocation := "command-runner.jar"
	args := []string{"spark-submit", "--class", req.Class, req.Jar, "10"}
	hadoopConfig := emr.HadoopJarStepConfig{
		Args: args,
		Jar:  &jarlocation,
	}

	stepCfg := emr.StepConfig{
		Name:            &name,
		HadoopJarStep:   &hadoopConfig,
		ActionOnFailure: emr.ActionOnFailureContinue,
	}

	input := emr.AddJobFlowStepsInput{
		JobFlowId: &req.Cluster,
		Steps:     []emr.StepConfig{stepCfg},
	}

	request := c.AddJobFlowStepsRequest(&input)
	response, err := request.Send()
	if err != nil {
		log.Printf("ERROR SparkSubmit: %s\n", err.Error())
		return err
	}

	if len(response.StepIds) != 1 {
		log.Printf("ERROR SparkSubmit: spected 1 StepId in response. Actual: %d\n", len(response.StepIds))
		return errors.New("Invalid StepIds response from EMR")
	}

	props := make(map[string]string)
	props["STEP"] = response.StepIds[0]
	props["ID"] = jobID
	props["STATE"] = "SUBMITTED"

	err = zk.UpdateJob(s.participant.GetConnection(), jobID, props)
	if err != nil {
		return err
	}

	resp := pb.SparkSubmitResponse{Message: jobID}
	err = stream.Send(&resp)
	if err != nil {
		log.Printf("ERROR SparkSubmit: %s\n", err.Error())
		return err
	}

	return nil
}

func (s *Server) DescribeJob(ctx context.Context, req *pb.DescribeJobRequest) (*pb.DescribeJobResponse, error) {
	log.Printf("BEGIN DescribeJob. Job: %s\n", req.Job)
	defer log.Printf("END DescribeJob. Job: %s\n", req.Job)

	jr, err := zk.GetJob(s.participant.GetConnection(), req.Job)
	if err != nil {
		return nil, err
	}

	cluster := jr.GetCluster()
	step := jr.GetStep()
	state := jr.GetState()

	log.Printf("DescribeJob. ID: %s, cluster: %s, step: %s, state: %s",
		req.Job, cluster, step, state)

	resp := pb.DescribeJobResponse{
		Job:     req.Job,
		Cluster: cluster,
		Step:    step,
		State:   state,
		Url:     fmt.Sprintf("https://air/jj/%s", req.Job),
	}

	return &resp, nil
}

func (s *Server) CreateJob(ctx context.Context, req *pb.CreateJobRequest) (*pb.CreateJobResponse, error)  {
	data := req.Data
	log.Printf("BEGIN Server.CreateJob. Data: %v\n", data)
	defer log.Printf("END Server.CreateJob. Data: %v\n", data)

	return nil, nil
}

func (s *Server) ListJobs(ctx context.Context, req *pb.Empty) (*pb.ListJobsResponse, error) {
	log.Printf("BEGIN Server.ListJobs")
	defer log.Printf("END Server.ListJobs")

	jobs, err := zk.ListJobs(s.participant.GetConnection())
	if err != nil {
		return nil, err
	}

	result := make([]*pb.DescribeJobResponse, len(jobs))
	for i, j := range jobs {
		result[i] = &pb.DescribeJobResponse{
			Job:     j.GetID(),
			Cluster: j.GetCluster(),
			Step:    j.GetStep(),
			State:   j.GetState(),
			Url:     fmt.Sprintf("https://air/jj/%s", j.GetID()),
		}
	}

	return &pb.ListJobsResponse{Jobs: result}, nil
}

func (s *Server) DescribeCluster(ctx context.Context, req *pb.DescribeClusterRequest) (*pb.DescribeClusterResponse, error) {
	log.Printf("BEGIN DescribeCluster. Cluster: %s\n", req.Cluster)
	defer log.Printf("END DescribeCluster. Cluster: %s\n", req.Cluster)

	cfg, err := external.LoadDefaultAWSConfig()
	if err != nil {
		log.Printf("ERROR CreateCluster: %s\n", err.Error())
		return nil, err
	}
	cfg.Region = endpoints.UsEast1RegionID

	c := emr.New(cfg)
	cluster := req.Cluster
	input := emr.DescribeClusterInput{
		ClusterId: &cluster,
	}

	request := c.DescribeClusterRequest(&input)
	response, err := request.Send()
	if err != nil {
		log.Printf("DescribeCluster: request.Send(): %s", err.Error())
		return nil, err
	}

	resp := pb.DescribeClusterResponse{
		Cluster:           req.Cluster,
		ScaleDownBehavior: string(response.Cluster.ScaleDownBehavior),
	}

	if response.Cluster.Name != nil {
		resp.Name = *response.Cluster.Name
	}

	if len(response.Cluster.Applications) > 0 {
		resp.Applications = make([]string, len(response.Cluster.Applications))
		for i, a := range response.Cluster.Applications {
			resp.Applications[i] = fmt.Sprintf("%s:%s", *a.Name, *a.Version)
		}
	}

	if response.Cluster.AutoTerminate != nil {
		resp.AutoTerminate = *response.Cluster.AutoTerminate
	}

	if response.Cluster.NormalizedInstanceHours != nil {
		resp.NormalizedInstanceHours = *response.Cluster.NormalizedInstanceHours
	}

	if response.Cluster.ReleaseLabel != nil {
		resp.ReleaseLabel = *response.Cluster.ReleaseLabel
	}

	if response.Cluster.RunningAmiVersion != nil {
		resp.RunningAmiVersion = *response.Cluster.RunningAmiVersion
	}

	if response.Cluster.Status != nil {
		resp.State = string(response.Cluster.Status.State)
	}

	if response.Cluster.MasterPublicDnsName != nil {
		resp.MasterPublicDnsName = *response.Cluster.MasterPublicDnsName
	}

	if response.Cluster.InstanceCollectionType == emr.InstanceCollectionTypeInstanceGroup {
		resp.InstanceCollectionType = string(emr.InstanceCollectionTypeInstanceGroup)
		input2 := emr.ListInstanceGroupsInput{ClusterId: &req.Cluster}
		request2 := c.ListInstanceGroupsRequest(&input2)
		response2, err := request2.Send()
		if err != nil {
			log.Printf("ERROR DescribeCluster: failed to ListInstanceGroups: %s", err.Error())
			return &resp, nil
		}

		for _, i := range response2.InstanceGroups {
			switch i.InstanceGroupType {
			case emr.InstanceGroupTypeCore:
				resp.CoreInstanceType = *i.InstanceType
				resp.CoreRunningInstanceCount = *i.RunningInstanceCount

			case emr.InstanceGroupTypeMaster:
				resp.MasterInstanceType = *i.InstanceType
				resp.MasterRunningInstanceCount = *i.RunningInstanceCount
			}
		}

	} else if response.Cluster.InstanceCollectionType == emr.InstanceCollectionTypeInstanceFleet {
		resp.InstanceCollectionType = string(emr.InstanceCollectionTypeInstanceFleet)

		input2 := emr.ListInstanceFleetsInput{ClusterId: &req.Cluster}
		request2 := c.ListInstanceFleetsRequest(&input2)
		response2, err := request2.Send()
		if err != nil {
			log.Printf("ERROR DescribeCluster: failed to ListInstanceGroups: %s", err.Error())
			return &resp, nil
		}

		for _, i := range response2.InstanceFleets {
			println(i.String())
		}
	}

	log.Printf("DescribeCluster: response: %v\n", resp)

	return &resp, nil
}
