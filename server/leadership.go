package server

import (
	"log"
	"strings"
	"time"

	"github.com/docker/leadership"
	"github.com/docker/libkv"
	"github.com/docker/libkv/store"
	"github.com/docker/libkv/store/zookeeper"
)

type LeaderElectionCallback func() error

type Leadership struct {
	electionNode     string
	nodeName         string
	zkSvr            string
	store            store.Store
	candidate        *leadership.Candidate
	OnBecomeLeader   LeaderElectionCallback
	OnBecomeFollower LeaderElectionCallback
	isLeader         bool
}

func NewLeadership(zkSvr string, electionNode string, node string, onBecomeLeader, onBecomeFollower LeaderElectionCallback) *Leadership {
	zookeeper.Register()

	store, err := libkv.NewStore(store.ZK, strings.Split(zkSvr, ","), &store.Config{
		ConnectionTimeout: 10 * time.Second})
	if err != nil {
		panic(err)
	}

	store.Put(electionNode, []byte(""), nil)

	l := Leadership{
		zkSvr:            zkSvr,
		store:            store,
		electionNode:     electionNode,
		nodeName:         node,
		OnBecomeLeader:   onBecomeLeader,
		OnBecomeFollower: onBecomeFollower,
	}

	return &l
}

func (l *Leadership) JoinElection() {
	l.candidate = leadership.NewCandidate(l.store, l.electionNode, l.nodeName, 15*time.Second)
	electedCh, _ := l.candidate.RunForElection()

	for isElected := range electedCh {
		// This loop will run every time there is a change in our leadership
		// status.

		if isElected {
			// We won the election - we are now the leader.
			// Let's do leader stuff, for example, sleep for a while.
			l.isLeader = true
			l.OnBecomeLeader()
		} else {
			// We lost the election but are still running for leadership.
			// `elected == false` is the default state and is the first event
			// we'll receive from the channel. After a successful election,
			// this event can get triggered if someone else steals the
			// leadership or if we resign.
			if l.isLeader {
				log.Printf("%s becomes follower", l.nodeName)
				l.isLeader = false
				l.OnBecomeFollower()
			}
		}
	}
}

func (l *Leadership) Stop() {
	l.candidate.Stop()
}

func (l *Leadership) Resign() {
	l.candidate.Resign()
}
