
VERSION=`cat VERSION`

.PHONY: setup
setup: ## Install all the build and lint dependencies
	go get -u github.com/pointlander/peg
	go get -u github.com/golang/protobuf/protoc-gen-go
	go get -u github.com/alecthomas/gometalinter
	go get -u golang.org/x/tools/cmd/cover
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	gometalinter --install --update
	brew install protobuf

codegen:
	# generate a static version number for the source code
	sed "s/{{VERSION}}/${VERSION}/" ./common/version.go.template > ./common/version.go

	# generate parsers for operator commands
	peg -inline -switch yarn/am/am.peg

	# generate protobuffer stubs for golang
	protoc -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
	    --proto_path=./pb --go_out=plugins=grpc:./pb \
	    ./pb/jj.proto

	# generate REST gateway / reverse proxy
	protoc -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
	    --grpc-gateway_out=logtostderr=true:./pb \
	    --proto_path=./pb \
	    ./pb/jj.proto

	# generate Swagger definitions for REST gateway / reverse proxy
	protoc -I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
	    --swagger_out=logtostderr=true:./pb \
	    --proto_path=./pb \
	    ./pb/jj.proto


.PHONY: fmt
fmt: ## Run goimports on all go files
	find . -name '*.go' -not -wholename './vendor/*' -not -name "*.peg.go" | while read -r file; do goimports -w "$$file"; done

.PHONY: lint
lint: ## Run all the linters
	gometalinter --vendor --disable-all \
		--enable=deadcode \
		--enable=ineffassign \
		--enable=gosimple \
		--enable=staticcheck \
		--enable=gofmt \
		--enable=goimports \
		--enable=misspell \
		--enable=errcheck \
		--enable=vet \
		--enable=vetshadow \
		--deadline=10m \
		./...

.PHONY: test
test: codegen ## run short unit tests
	go test ./... -short


.PHONY: install
install: test ## install binary to $PATH
	go install ./cli/jj
	go install ./cli/j

.PHONY: build
build: test ## Build binaries for linux/amd64 and darwin/amd64
	gox -osarch="linux/amd64 darwin/amd64" -output="./releases/jj.{{.OS}}_{{.Arch}}" ./cli/jj/
	gox -osarch="linux/amd64 darwin/amd64" -output="./releases/j.{{.OS}}_{{.Arch}}" ./cli/j/


.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'