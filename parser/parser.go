package parser

import "github.com/pointlander/peg/tree"

func ParseApplicationMasterLog(line string) error {
	p := &Peg{Tree: tree.New(true, true, false), Buffer: string(line)}
	p.Init()
	p.Reset()
	err := p.Parse()
	if err != nil {
		return err
	}

	tokens := p.Tokens()

	for _, t := range tokens {

		if t.pegRule == ruleappStatus {
			println("appStatus: " + line[t.begin:t.end])
		}

		if t.pegRule == ruleexitCode {
			println("exitCode: " + line[t.begin:t.end])
		}
	}

	return nil
}
