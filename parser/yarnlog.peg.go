package parser

//go:generate peg -inline -switch parser/am.peg

import (
	"fmt"
	"github.com/pointlander/peg/tree"
	"math"
	"sort"
	"strconv"
)

const endSymbol rune = 1114112

/* The rule types inferred from the grammar are below. */
type pegRule uint8

const (
	ruleUnknown pegRule = iota
	rulelogline
	ruleamlog
	ruleamcontent
	ruleattempt
	rulefinalstatus
	rulecontent
	ruleexitCode
	ruleappStatus
	ruledd
	rulets
)

var rul3s = [...]string{
	"Unknown",
	"logline",
	"amlog",
	"amcontent",
	"attempt",
	"finalstatus",
	"content",
	"exitCode",
	"appStatus",
	"dd",
	"ts",
}

type token32 struct {
	pegRule
	begin, end uint32
}

func (t *token32) String() string {
	return fmt.Sprintf("\x1B[34m%v\x1B[m %v %v", rul3s[t.pegRule], t.begin, t.end)
}

type node32 struct {
	token32
	up, next *node32
}

func (node *node32) print(pretty bool, buffer string) {
	var print func(node *node32, depth int)
	print = func(node *node32, depth int) {
		for node != nil {
			for c := 0; c < depth; c++ {
				fmt.Printf(" ")
			}
			rule := rul3s[node.pegRule]
			quote := strconv.Quote(string(([]rune(buffer)[node.begin:node.end])))
			if !pretty {
				fmt.Printf("%v %v\n", rule, quote)
			} else {
				fmt.Printf("\x1B[34m%v\x1B[m %v\n", rule, quote)
			}
			if node.up != nil {
				print(node.up, depth+1)
			}
			node = node.next
		}
	}
	print(node, 0)
}

func (node *node32) Print(buffer string) {
	node.print(false, buffer)
}

func (node *node32) PrettyPrint(buffer string) {
	node.print(true, buffer)
}

type tokens32 struct {
	tree []token32
}

func (t *tokens32) Trim(length uint32) {
	t.tree = t.tree[:length]
}

func (t *tokens32) Print() {
	for _, token := range t.tree {
		fmt.Println(token.String())
	}
}

func (t *tokens32) AST() *node32 {
	type element struct {
		node *node32
		down *element
	}
	tokens := t.Tokens()
	var stack *element
	for _, token := range tokens {
		if token.begin == token.end {
			continue
		}
		node := &node32{token32: token}
		for stack != nil && stack.node.begin >= token.begin && stack.node.end <= token.end {
			stack.node.next = node.up
			node.up = stack.node
			stack = stack.down
		}
		stack = &element{node: node, down: stack}
	}
	if stack != nil {
		return stack.node
	}
	return nil
}

func (t *tokens32) PrintSyntaxTree(buffer string) {
	t.AST().Print(buffer)
}

func (t *tokens32) PrettyPrintSyntaxTree(buffer string) {
	t.AST().PrettyPrint(buffer)
}

func (t *tokens32) Add(rule pegRule, begin, end, index uint32) {
	if tree := t.tree; int(index) >= len(tree) {
		expanded := make([]token32, 2*len(tree))
		copy(expanded, tree)
		t.tree = expanded
	}
	t.tree[index] = token32{
		pegRule: rule,
		begin:   begin,
		end:     end,
	}
}

func (t *tokens32) Tokens() []token32 {
	return t.tree
}

type Peg struct {
	*tree.Tree

	Buffer string
	buffer []rune
	rules  [11]func() bool
	parse  func(rule ...int) error
	reset  func()
	Pretty bool
	tokens32
}

func (p *Peg) Parse(rule ...int) error {
	return p.parse(rule...)
}

func (p *Peg) Reset() {
	p.reset()
}

type textPosition struct {
	line, symbol int
}

type textPositionMap map[int]textPosition

func translatePositions(buffer []rune, positions []int) textPositionMap {
	length, translations, j, line, symbol := len(positions), make(textPositionMap, len(positions)), 0, 1, 0
	sort.Ints(positions)

search:
	for i, c := range buffer {
		if c == '\n' {
			line, symbol = line+1, 0
		} else {
			symbol++
		}
		if i == positions[j] {
			translations[positions[j]] = textPosition{line, symbol}
			for j++; j < length; j++ {
				if i != positions[j] {
					continue search
				}
			}
			break search
		}
	}

	return translations
}

type parseError struct {
	p   *Peg
	max token32
}

func (e *parseError) Error() string {
	tokens, error := []token32{e.max}, "\n"
	positions, p := make([]int, 2*len(tokens)), 0
	for _, token := range tokens {
		positions[p], p = int(token.begin), p+1
		positions[p], p = int(token.end), p+1
	}
	translations := translatePositions(e.p.buffer, positions)
	format := "parse error near %v (line %v symbol %v - line %v symbol %v):\n%v\n"
	if e.p.Pretty {
		format = "parse error near \x1B[34m%v\x1B[m (line %v symbol %v - line %v symbol %v):\n%v\n"
	}
	for _, token := range tokens {
		begin, end := int(token.begin), int(token.end)
		error += fmt.Sprintf(format,
			rul3s[token.pegRule],
			translations[begin].line, translations[begin].symbol,
			translations[end].line, translations[end].symbol,
			strconv.Quote(string(e.p.buffer[begin:end])))
	}

	return error
}

func (p *Peg) PrintSyntaxTree() {
	if p.Pretty {
		p.tokens32.PrettyPrintSyntaxTree(p.Buffer)
	} else {
		p.tokens32.PrintSyntaxTree(p.Buffer)
	}
}

func (p *Peg) Init() {
	var (
		max                  token32
		position, tokenIndex uint32
		buffer               []rune
	)
	p.reset = func() {
		max = token32{}
		position, tokenIndex = 0, 0

		p.buffer = []rune(p.Buffer)
		if len(p.buffer) == 0 || p.buffer[len(p.buffer)-1] != endSymbol {
			p.buffer = append(p.buffer, endSymbol)
		}
		buffer = p.buffer
	}
	p.reset()

	_rules := p.rules
	tree := tokens32{tree: make([]token32, math.MaxInt16)}
	p.parse = func(rule ...int) error {
		r := 1
		if len(rule) > 0 {
			r = rule[0]
		}
		matches := p.rules[r]()
		p.tokens32 = tree
		if matches {
			p.Trim(tokenIndex)
			return nil
		}
		return &parseError{p, max}
	}

	add := func(rule pegRule, begin uint32) {
		tree.Add(rule, begin, position, tokenIndex)
		tokenIndex++
		if begin != position && position > max.end {
			max = token32{rule, begin, position}
		}
	}

	matchDot := func() bool {
		if buffer[position] != endSymbol {
			position++
			return true
		}
		return false
	}

	/*matchChar := func(c byte) bool {
		if buffer[position] == c {
			position++
			return true
		}
		return false
	}*/

	/*matchRange := func(lower byte, upper byte) bool {
		if c := buffer[position]; c >= lower && c <= upper {
			position++
			return true
		}
		return false
	}*/

	_rules = [...]func() bool{
		nil,
		/* 0 logline <- <((amlog / (ts ' ' content)) !.)> */
		func() bool {
			position0, tokenIndex0 := position, tokenIndex
			{
				position1 := position
				{
					position2, tokenIndex2 := position, tokenIndex
					{
						position4 := position
						if !_rules[rulets]() {
							goto l3
						}
						if buffer[position] != rune(' ') {
							goto l3
						}
						position++
						{
							position5 := position
							if buffer[position] != rune('I') {
								goto l3
							}
							position++
							if buffer[position] != rune('N') {
								goto l3
							}
							position++
							if buffer[position] != rune('F') {
								goto l3
							}
							position++
							if buffer[position] != rune('O') {
								goto l3
							}
							position++
							if buffer[position] != rune(' ') {
								goto l3
							}
							position++
							if buffer[position] != rune('A') {
								goto l3
							}
							position++
							if buffer[position] != rune('p') {
								goto l3
							}
							position++
							if buffer[position] != rune('p') {
								goto l3
							}
							position++
							if buffer[position] != rune('l') {
								goto l3
							}
							position++
							if buffer[position] != rune('i') {
								goto l3
							}
							position++
							if buffer[position] != rune('c') {
								goto l3
							}
							position++
							if buffer[position] != rune('a') {
								goto l3
							}
							position++
							if buffer[position] != rune('t') {
								goto l3
							}
							position++
							if buffer[position] != rune('i') {
								goto l3
							}
							position++
							if buffer[position] != rune('o') {
								goto l3
							}
							position++
							if buffer[position] != rune('n') {
								goto l3
							}
							position++
							if buffer[position] != rune('M') {
								goto l3
							}
							position++
							if buffer[position] != rune('a') {
								goto l3
							}
							position++
							if buffer[position] != rune('s') {
								goto l3
							}
							position++
							if buffer[position] != rune('t') {
								goto l3
							}
							position++
							if buffer[position] != rune('e') {
								goto l3
							}
							position++
							if buffer[position] != rune('r') {
								goto l3
							}
							position++
							if buffer[position] != rune(':') {
								goto l3
							}
							position++
							{
								position6, tokenIndex6 := position, tokenIndex
								{
									position8 := position
									if buffer[position] != rune('A') {
										goto l7
									}
									position++
									if buffer[position] != rune('p') {
										goto l7
									}
									position++
									if buffer[position] != rune('p') {
										goto l7
									}
									position++
									if buffer[position] != rune('l') {
										goto l7
									}
									position++
									if buffer[position] != rune('i') {
										goto l7
									}
									position++
									if buffer[position] != rune('c') {
										goto l7
									}
									position++
									if buffer[position] != rune('a') {
										goto l7
									}
									position++
									if buffer[position] != rune('t') {
										goto l7
									}
									position++
									if buffer[position] != rune('i') {
										goto l7
									}
									position++
									if buffer[position] != rune('o') {
										goto l7
									}
									position++
									if buffer[position] != rune('n') {
										goto l7
									}
									position++
									if buffer[position] != rune('A') {
										goto l7
									}
									position++
									if buffer[position] != rune('t') {
										goto l7
									}
									position++
									if buffer[position] != rune('t') {
										goto l7
									}
									position++
									if buffer[position] != rune('e') {
										goto l7
									}
									position++
									if buffer[position] != rune('m') {
										goto l7
									}
									position++
									if buffer[position] != rune('p') {
										goto l7
									}
									position++
									if buffer[position] != rune('t') {
										goto l7
									}
									position++
									if buffer[position] != rune('I') {
										goto l7
									}
									position++
									if buffer[position] != rune('d') {
										goto l7
									}
									position++
									if buffer[position] != rune(':') {
										goto l7
									}
									position++
									if buffer[position] != rune(' ') {
										goto l7
									}
									position++
									if !_rules[rulecontent]() {
										goto l7
									}
									add(ruleattempt, position8)
								}
								goto l6
							l7:
								position, tokenIndex = position6, tokenIndex6
								{
									position9 := position
									if buffer[position] != rune(' ') {
										goto l3
									}
									position++
								l10:
									{
										position11, tokenIndex11 := position, tokenIndex
										if buffer[position] != rune(' ') {
											goto l11
										}
										position++
										goto l10
									l11:
										position, tokenIndex = position11, tokenIndex11
									}
									if buffer[position] != rune('F') {
										goto l3
									}
									position++
									if buffer[position] != rune('i') {
										goto l3
									}
									position++
									if buffer[position] != rune('n') {
										goto l3
									}
									position++
									if buffer[position] != rune('a') {
										goto l3
									}
									position++
									if buffer[position] != rune('l') {
										goto l3
									}
									position++
									if buffer[position] != rune(' ') {
										goto l3
									}
									position++
									if buffer[position] != rune('a') {
										goto l3
									}
									position++
									if buffer[position] != rune('p') {
										goto l3
									}
									position++
									if buffer[position] != rune('p') {
										goto l3
									}
									position++
									if buffer[position] != rune(' ') {
										goto l3
									}
									position++
									if buffer[position] != rune('s') {
										goto l3
									}
									position++
									if buffer[position] != rune('t') {
										goto l3
									}
									position++
									if buffer[position] != rune('a') {
										goto l3
									}
									position++
									if buffer[position] != rune('t') {
										goto l3
									}
									position++
									if buffer[position] != rune('u') {
										goto l3
									}
									position++
									if buffer[position] != rune('s') {
										goto l3
									}
									position++
									if buffer[position] != rune(':') {
										goto l3
									}
									position++
									if buffer[position] != rune(' ') {
										goto l3
									}
									position++
									{
										position12 := position
									l13:
										{
											position14, tokenIndex14 := position, tokenIndex
											if c := buffer[position]; c < rune('A') || c > rune('Z') {
												goto l14
											}
											position++
											goto l13
										l14:
											position, tokenIndex = position14, tokenIndex14
										}
										add(ruleappStatus, position12)
									}
									if buffer[position] != rune(',') {
										goto l3
									}
									position++
									if buffer[position] != rune(' ') {
										goto l3
									}
									position++
									if buffer[position] != rune('e') {
										goto l3
									}
									position++
									if buffer[position] != rune('x') {
										goto l3
									}
									position++
									if buffer[position] != rune('i') {
										goto l3
									}
									position++
									if buffer[position] != rune('t') {
										goto l3
									}
									position++
									if buffer[position] != rune('C') {
										goto l3
									}
									position++
									if buffer[position] != rune('o') {
										goto l3
									}
									position++
									if buffer[position] != rune('d') {
										goto l3
									}
									position++
									if buffer[position] != rune('e') {
										goto l3
									}
									position++
									if buffer[position] != rune(':') {
										goto l3
									}
									position++
									if buffer[position] != rune(' ') {
										goto l3
									}
									position++
									{
										position15 := position
										if c := buffer[position]; c < rune('0') || c > rune('9') {
											goto l3
										}
										position++
									l16:
										{
											position17, tokenIndex17 := position, tokenIndex
											if c := buffer[position]; c < rune('0') || c > rune('9') {
												goto l17
											}
											position++
											goto l16
										l17:
											position, tokenIndex = position17, tokenIndex17
										}
										add(ruleexitCode, position15)
									}
									add(rulefinalstatus, position9)
								}
							}
						l6:
							add(ruleamcontent, position5)
						}
						add(ruleamlog, position4)
					}
					goto l2
				l3:
					position, tokenIndex = position2, tokenIndex2
					if !_rules[rulets]() {
						goto l0
					}
					if buffer[position] != rune(' ') {
						goto l0
					}
					position++
					if !_rules[rulecontent]() {
						goto l0
					}
				}
			l2:
				{
					position18, tokenIndex18 := position, tokenIndex
					if !matchDot() {
						goto l18
					}
					goto l0
				l18:
					position, tokenIndex = position18, tokenIndex18
				}
				add(rulelogline, position1)
			}
			return true
		l0:
			position, tokenIndex = position0, tokenIndex0
			return false
		},
		/* 1 amlog <- <(ts ' ' amcontent)> */
		nil,
		/* 2 amcontent <- <('I' 'N' 'F' 'O' ' ' 'A' 'p' 'p' 'l' 'i' 'c' 'a' 't' 'i' 'o' 'n' 'M' 'a' 's' 't' 'e' 'r' ':' (attempt / finalstatus))> */
		nil,
		/* 3 attempt <- <('A' 'p' 'p' 'l' 'i' 'c' 'a' 't' 'i' 'o' 'n' 'A' 't' 't' 'e' 'm' 'p' 't' 'I' 'd' ':' ' ' content)> */
		nil,
		/* 4 finalstatus <- <(' '+ ('F' 'i' 'n' 'a' 'l' ' ' 'a' 'p' 'p' ' ' 's' 't' 'a' 't' 'u' 's' ':' ' ') appStatus (',' ' ' 'e' 'x' 'i' 't' 'C' 'o' 'd' 'e' ':' ' ') exitCode)> */
		nil,
		/* 5 content <- <.*> */
		func() bool {
			{
				position24 := position
			l25:
				{
					position26, tokenIndex26 := position, tokenIndex
					if !matchDot() {
						goto l26
					}
					goto l25
				l26:
					position, tokenIndex = position26, tokenIndex26
				}
				add(rulecontent, position24)
			}
			return true
		},
		/* 6 exitCode <- <[0-9]+> */
		nil,
		/* 7 appStatus <- <[A-Z]*> */
		nil,
		/* 8 dd <- <([0-9] [0-9])> */
		func() bool {
			position29, tokenIndex29 := position, tokenIndex
			{
				position30 := position
				if c := buffer[position]; c < rune('0') || c > rune('9') {
					goto l29
				}
				position++
				if c := buffer[position]; c < rune('0') || c > rune('9') {
					goto l29
				}
				position++
				add(ruledd, position30)
			}
			return true
		l29:
			position, tokenIndex = position29, tokenIndex29
			return false
		},
		/* 9 ts <- <(dd '/' dd '/' dd ' ' dd ':' dd ':' dd)> */
		func() bool {
			position31, tokenIndex31 := position, tokenIndex
			{
				position32 := position
				if !_rules[ruledd]() {
					goto l31
				}
				if buffer[position] != rune('/') {
					goto l31
				}
				position++
				if !_rules[ruledd]() {
					goto l31
				}
				if buffer[position] != rune('/') {
					goto l31
				}
				position++
				if !_rules[ruledd]() {
					goto l31
				}
				if buffer[position] != rune(' ') {
					goto l31
				}
				position++
				if !_rules[ruledd]() {
					goto l31
				}
				if buffer[position] != rune(':') {
					goto l31
				}
				position++
				if !_rules[ruledd]() {
					goto l31
				}
				if buffer[position] != rune(':') {
					goto l31
				}
				position++
				if !_rules[ruledd]() {
					goto l31
				}
				add(rulets, position32)
			}
			return true
		l31:
			position, tokenIndex = position31, tokenIndex31
			return false
		},
	}
	p.rules = _rules
}
