package models

import "time"

// EmrClustersAdmin models the JSON from endpoint /v1/admin/clusters
type EmrClustersAdmin []struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Status struct {
		State             string `json:"state"`
		StateChangeReason struct {
			Message string `json:"message"`
		} `json:"stateChangeReason"`
		Timeline struct {
			CreationDateTime struct {
				Seconds int `json:"seconds"`
				Nanos   int `json:"nanos"`
			} `json:"creationDateTime"`
			ReadyDateTime struct {
				Seconds int `json:"seconds"`
				Nanos   int `json:"nanos"`
			} `json:"readyDateTime"`
		} `json:"timeline"`
	} `json:"status"`
	NormalizedInstanceHours int `json:"normalizedInstanceHours"`
}

// EmrClustersPublic models the JSON from endpoint /v1/clusters
type EmrClustersPublic struct {
	Data []struct {
		ID                       string    `json:"id"`
		Name                     string    `json:"name"`
		State                    string    `json:"state"`
		CreationDateTime         time.Time `json:"creationDateTime"`
		ReadyDateTime            time.Time `json:"readyDateTime,omitempty"`
		EndDateTime              time.Time `json:"endDateTime,omitempty"`
		NormalizedInstanceHours  int       `json:"normalizedInstanceHours"`
		ReleaseLabel             string    `json:"releaseLabel,omitempty"`
		CoreInstanceCount        int       `json:"coreInstanceCount"`
		CoreInstanceGroupState   string    `json:"coreInstanceGroupState,omitempty"`
		CoreInstanceType         string    `json:"coreInstanceType,omitempty"`
		MasterInstanceCount      int       `json:"masterInstanceCount"`
		MasterInstanceGroupState string    `json:"masterInstanceGroupState"`
		MasterInstanceType       string    `json:"masterInstanceType"`
		TaskInstanceCount        int       `json:"taskInstanceCount"`
		TaskInstanceGroupState   string    `json:"taskInstanceGroupState,omitempty"`
		TaskInstanceType         string    `json:"taskInstanceType,omitempty"`
		MasterPublicDNSName      string    `json:"masterPublicDnsName"`
	} `json:"data"`
}
