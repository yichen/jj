package models


type TaskState struct {
	ID           string        `json:"id"`
	DeltaList    []interface{} `json:"_deltaList"`
	SimpleFields struct {
		LASTRUNBEGINAT string `json:"LAST_RUN_BEGIN_AT"`
		LASTRUNENDAT   string `json:"LAST_RUN_END_AT"`
		LASTRUNRESULT  string `json:"LAST_RUN_RESULT"`
		LASTRUNEXCEPTION string `json:"LAST_RUN_EXCEPTION"`
	} `json:"simpleFields"`
	MapFields struct {
	} `json:"mapFields"`
	ListFields struct {
	} `json:"listFields"`
	Serializer struct {
	} `json:"_serializer"`
	Version        int `json:"_version"`
	CreationTime   int `json:"_creationTime"`
	ModifiedTime   int `json:"_modifiedTime"`
	EphemeralOwner int `json:"_ephemeralOwner"`
}
