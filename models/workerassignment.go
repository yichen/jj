package models

type Assignment map[string][]string

type WorkerAssignment struct {
	ID           string        `json:"id"`
	SimpleFields struct {
		LASTRUNBEGINAT   string `json:"LAST_RUN_BEGIN_AT"`
		LASTRUNENDAT     string `json:"LAST_RUN_END_AT"`
		LASTRUNEXCEPTION string `json:"LAST_RUN_EXCEPTION"`
		LASTRUNRESULT    string `json:"LAST_RUN_RESULT"`
	} `json:"simpleFields"`
	MapFields struct {
	} `json:"mapFields"`
	ListFields Assignment `json:"listFields"`
}
