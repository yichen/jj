package common

const (
	BACKEND_WORKER_STAGING = "https://jobserver-worker-staging.d.musta.ch"
	BACKEND_WORKER_PRODUCTION = "https://jobserver-worker.d.musta.ch"
	BACKEND_MASTER_STAGING = "https://jobserver-master-staging.d.musta.ch"
	BACKEND_MASTER_PRODUCTION = "https://jobserver-master.d.musta.ch"
)
