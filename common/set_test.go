package common

import (
	"testing"
)

func TestSet_Add(t *testing.T) {
	t.Parallel()

	var s1 = NewLinkedSet()

	s1.Push("a")
	s1.Push("b")
	s1.Push("c")

	if s1.Size() != 3 {
		t.FailNow()
	}

	s1.Push("a")
	s1.Push("b")
	s1.Push("c")

	if s1.Size() != 3 {
		t.FailNow()
	}

	s1.Remove("a")
	s1.Remove("b")

	if s1.Size() != 1 {
		t.FailNow()
	}

	s1.Remove("d")
	if s1.Size() != 1 {
		t.FailNow()
	}
}

func TestLinkedSet_Pop(t *testing.T) {
	t.Parallel()

	var s1 = NewLinkedSet()

	s1.Push("a")
	s1.Push("b")
	s1.Push("c")

	v := s1.Peek()
	if v != "a" {
		t.FailNow()
	}

	if s1.Size() != 3 {
		t.FailNow()
	}

	v = s1.Pop()
	if v != "a" {
		t.FailNow()
	}

	v = s1.Pop()
	if v != "b" {
		t.FailNow()
	}
}

func TestLinkedSet_Exists(t *testing.T) {
	t.Parallel()

	var s1 = NewLinkedSet()

	s1.Push("a")
	s1.Push("b")
	s1.Push("c")

	if !s1.Contains("a") {
		t.FailNow()
	}

	r := s1.Pop()

	if r != "a" {
		t.FailNow()
	}

	if s1.Contains("a") {
		t.FailNow()
	}

	if s1.Contains("nothing") {
		t.FailNow()
	}

	all := s1.GetAll()
	if len(all) != 2 {
		t.FailNow()
	}
}

func TestLinkedSet_Equals(t *testing.T) {
	t.Parallel()

	var s1 = NewLinkedSet()
	var s2 = NewLinkedSet()
	s1.Push("a")
	s1.Push("b")

	s2.Push("b")
	s2.Push("a")

	if !s1.Equals(s2) {
		t.FailNow()
	}

	s2.Push("c")
	if s1.Equals(s2) {
		t.FailNow()
	}
}

func TestLinkedSet_RemoveAll(t *testing.T) {
	t.Parallel()

	var s1 = NewLinkedSet()
	var s2 = NewLinkedSet()
	s1.Push("a")
	s1.Push("b")
	s1.Push("c")
	s1.Push("d")

	s2.Push("b")
	s2.Push("a")
	s2.Push("e")
	s2.Push("f")

	s3 := s1.RemoveAll(s2)
	if s3.Size() != 2 {
		t.FailNow()
	}

	if !s3.Contains("c") || !s3.Contains("d") {
		t.FailNow()
	}
}

func TestLinkedSet_PushAll(t *testing.T) {
	t.Parallel()

	var s1 = NewLinkedSet()
	s1.PushAll("a", "a", "b", "b", "c")

	if s1.Size() != 3 {
		t.FailNow()
	}
}
