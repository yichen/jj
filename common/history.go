package common

// History is a data structure that keeps the most recent parsed logs with
type History struct {
	size    int
	content []string
	index   int
	inc     int
}

func NewHistory(size int) History {
	h := History{
		size:    size,
		index:   0,
		content: make([]string, size),
	}

	return h
}

func (h *History) Append(d string) {
	h.inc++

	h.content[h.index] = d
	h.index++

	if h.index == h.size {
		h.index = 0
	}
}

func (h History) GetPrevBy(pos int) string {
	if pos == 0 {
		return ""
	}

	if pos >= h.inc {
		return ""
	}

	p := h.index - pos
	if p < 0 {
		p += h.size
	}

	return h.content[p]
}
