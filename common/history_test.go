package common

import (
	"strconv"
	"strings"
	"testing"
)

func TestHistory_Append(t *testing.T) {
	h := NewHistory(5)

	for i := 0; i < 5; i++ {
		h.Append(strconv.Itoa(i))
	}

	if strings.Compare(h.GetPrevBy(1), "4") != 0 {
		t.FailNow()
	}

	h.Append("5")

	if strings.Compare(h.GetPrevBy(1), "5") != 0 {
		t.FailNow()
	}

	if strings.Compare(h.GetPrevBy(2), "4") != 0 {
		t.FailNow()
	}

	if strings.Compare(h.GetPrevBy(3), "3") != 0 {
		t.FailNow()
	}

}
