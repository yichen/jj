package common

import (
	"container/list"
	"sort"
	"strings"

	"github.com/sasha-s/go-deadlock"
)

type LinkedSet interface {
	Push(item string)
	Pop() string
	Peek() string
	Remove(item string)
	Contains(item string) bool
	GetAll() []string
	Size() int
	Equals(o *linkedSet) bool
	RemoveAll(other *linkedSet)
}

type linkedSet struct {
	deadlock.RWMutex

	innerMap  map[string]*list.Element
	innerList *list.List
}

func NewLinkedSet() *linkedSet {
	s := linkedSet{
		innerMap:  make(map[string]*list.Element),
		innerList: list.New(),
	}

	return &s
}

func (s *linkedSet) Push(item string) {
	if item == "" {
		return
	}

	s.RWMutex.Lock()
	defer s.RWMutex.Unlock()

	if _, exists := s.innerMap[item]; exists {
		return
	}

	el := s.innerList.PushBack(item)
	s.innerMap[item] = el
}

func (s *linkedSet) PushAll(items ...string)  {
	for _, v := range items {
		s.Push(v)
	}
}

func (s *linkedSet) Size() int {
	s.RWMutex.RLock()
	defer s.RWMutex.RUnlock()
	return s.innerList.Len()
}

func (s *linkedSet) Remove(item string) {
	s.RWMutex.Lock()
	defer s.RWMutex.Unlock()

	if _, exists := s.innerMap[item]; !exists {
		return
	}

	el := s.innerMap[item]
	s.innerList.Remove(el)
	delete(s.innerMap, item)
}

func (s *linkedSet) Peek() string {
	s.RWMutex.RLock()
	defer s.RWMutex.RUnlock()

	el := s.innerList.Front()
	return el.Value.(string)
}

func (s *linkedSet) Pop() string {
	s.RWMutex.Lock()
	defer s.RWMutex.Unlock()

	el := s.innerList.Front()
	if el == nil {
		return ""
	}
	r := el.Value
	s.innerList.Remove(el)
	delete(s.innerMap, r.(string))
	if r == nil {
		return ""
	}
	return r.(string)
}

func (s *linkedSet) GetAll() []string {
	s.RWMutex.Lock()
	defer s.RWMutex.Unlock()

	var result []string
	for e := s.innerList.Front(); e != nil; e = e.Next() {
		// do something with e.Value
		result = append(result, e.Value.(string))
	}

	return result
}

func (s *linkedSet) RemoveAll(another *linkedSet) *linkedSet {

	result := NewLinkedSet()
	keys := s.GetAll()

	s.RWMutex.Lock()
	defer s.RWMutex.Unlock()

	for _, k := range keys {
		if !another.Contains(k) {
			result.Push(k)
		}
	}

	return result
}

func (s *linkedSet) Contains(item string) bool {
	if _, exists := s.innerMap[item]; exists {
		return true
	} else {
		return false
	}
}

func (s *linkedSet) Equals(o *linkedSet) bool {

	if s.Size() != o.Size() {
		return false
	}

	sl := s.GetAll()
	sort.Strings(sl)
	ol := o.GetAll()
	sort.Strings(ol)

	for i := range sl {
		if sl[i] != ol[i] {
			return false
		}
	}

	return true
}

func (s *linkedSet) String() string {
	sl := s.GetAll()
	sort.Strings(sl)

	return strings.Join(sl, ",")
}
