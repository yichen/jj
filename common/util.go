package common

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/user"
	"path"
	"path/filepath"
	"runtime"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/aws/endpoints"
	"github.com/aws/aws-sdk-go-v2/aws/external"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/s3/s3manager"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

const localCacheRoot = "/tmp/jobs"

func GetHomeDir() string {
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	return usr.HomeDir
}

func GetConfigPath() string {
	var configPath string
	if runtime.GOOS == "darwin" {
		configPath = fmt.Sprintf("%s/.jj", GetHomeDir())
	} else if runtime.GOOS == "linux" {
		hd, err := homedir.Dir()
		if err != nil {
			fmt.Println("failed to get home dir for Linux. Error: ", err.Error())
			configPath = "/tmp/jj"
		} else {
			configPath = hd
		}
	} else {
		configPath = "."
	}

	return configPath
}

func ReadConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("json")
	var configPath = GetConfigPath()
	configFileName := filepath.Join(configPath, "config.json")

	if configPath != "." {
		err := os.MkdirAll(configPath, os.ModePerm)
		if err != nil {
			fmt.Println("failed to create config path: ", configPath, ". Error: ", err.Error())
		}
	}

	viper.AddConfigPath(configPath)

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		if _, err := os.Stat(configPath); os.IsNotExist(err) {
			os.MkdirAll(configPath, os.ModePerm)
		}

		if _, err := os.Stat(configFileName); os.IsNotExist(err) {
			viper.Set("server", "")
			viper.WriteConfigAs(configFileName)
		}
	}
}

func DownloadStepLogs(job string, cluster string, step string) error {
	log.Printf("BEGIN DownloadStepLog. Job: %s", job)
	defer log.Printf("END DownloadStepLog. Job: %s", job)

	cfg, err := external.LoadDefaultAWSConfig()
	if err != nil {
		log.Printf("ListClusters: %s", err.Error())
		return err
	}
	cfg.Region = endpoints.UsEast1RegionID

	if _, err := os.Stat(localCacheRoot); os.IsNotExist(err) {
		err = os.Mkdir(localCacheRoot, os.ModePerm)
		if err != nil {
			log.Printf("ERROR DownloadStepLogs: failed to create directory %s", localCacheRoot)
			return err
		}
	}

	jobCache := path.Join(localCacheRoot, job)
	if _, err := os.Stat(jobCache); os.IsNotExist(err) {
		err = os.Mkdir(jobCache, os.ModePerm)
		if err != nil {
			log.Printf("ERROR DownloadStepLogs: failed to create directory %s. Error: %s\n", jobCache, err.Error())
			return err
		}
	}

	logFiles := []string{"controller.gz", "stderr.gz", "stdout.gz"}
	for _, l := range logFiles {
		localFile := path.Join(jobCache, l)
		file, err := os.Create(localFile)
		if err != nil {
			log.Printf("ERROR GetLog: unable to open file %s. Error: %s\n", localFile, err.Error())
			return err
		}

		downloader := s3manager.NewDownloader(cfg)

		b := "airbnb-hadoop-aws-internal-only"
		k := fmt.Sprintf("logs/%s/steps/%s/%s", cluster, step, l)

		input := s3.GetObjectInput{
			Bucket: aws.String(b),
			Key:    aws.String(k),
		}

		numBytes, err := downloader.Download(file, &input)
		if err != nil {
			log.Printf("ERROR GetLog: unable to download log file %s for job %s", l, job)
			continue
		}
		log.Printf("DownloadStepLog: downloaded %d bytes for file [%s] of job %s",
			numBytes, l, job)
	}

	return nil
}

func CatStepLogs(job string) chan string {
	output := make(chan string)

	jobCache := path.Join(localCacheRoot, job)
	logFiles := []string{"controller.gz", "stderr.gz", "stdout.gz"}

	go func() {
		for i, l := range logFiles {
			localFile := path.Join(jobCache, l)
			if _, err := os.Stat(localFile); os.IsNotExist(err) {
				continue
			}

			out, err := ZcatFile(localFile)
			if err != nil {
				continue
			}

			go func() {
				for lo := range out {
					output <- lo
				}
				if i == len(logFiles)-1 {
					close(output)
				}
			}()
		}
	}()

	return output
}

func ZcatFile(filePath string) (chan string, error) {
	out := make(chan string)
	cmd := exec.Command("/bin/bash", "-c", "zcat "+filePath)
	reader, err := cmd.StdoutPipe()
	if err != nil {
		log.Printf("ERROR catLog: failed to zcat log file: %s\n", filePath)
		return out, err
	}

	scanner := bufio.NewScanner(reader)
	go func() {
		for scanner.Scan() {
			txt := scanner.Text()
			out <- txt
		}
		close(out)
	}()

	err = cmd.Start()
	if err != nil {
		log.Printf("ERROR catLog: failed to zcat log file: %s\n", filePath)
		return out, err
	}

	go func() {
		err = cmd.Wait()
		if err != nil {
			log.Println("ERROR catLog: failed to wait for command to finish")
			close(out)
		}
	}()

	return out, nil
}

func GrepFile(pattern string, filePath string) (chan string, error) {
	out := make(chan string)
	cmd := exec.Command("/bin/bash", "-c", "grep "+pattern + " " + filePath)
	reader, err := cmd.StdoutPipe()
	if err != nil {
		log.Printf("ERROR catLog: failed to grep log file: %s\n", filePath)
		return out, err
	}

	scanner := bufio.NewScanner(reader)
	go func() {
		for scanner.Scan() {
			txt := scanner.Text()
			out <- txt
		}
		close(out)
	}()

	err = cmd.Start()
	if err != nil {
		log.Printf("ERROR catLog: failed to zcat log file: %s\n", filePath)
		return out, err
	}

	go func() {
		err = cmd.Wait()
		if err != nil {
			log.Println("ERROR catLog: failed to wait for command to finish")
			close(out)
		}
	}()

	return out, nil
}
